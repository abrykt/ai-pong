#include "gtest/gtest.h"

#include "object.hpp"

#include "line.hpp"

TEST(Object, vertical)
{
  Line test_line(TruePosition(0, 1), TruePosition(0, 3));
  const GridPositions& positions = test_line.get_grid_positions();
  EXPECT_FALSE(positions.get_position(0, 0));
  EXPECT_TRUE(positions.get_position(0, 1));
  EXPECT_TRUE(positions.get_position(0, 2));
  EXPECT_TRUE(positions.get_position(0, 3));
  EXPECT_FALSE(positions.get_position(0, 4));
}

TEST(Object, horizontal)
{
  Line test_line(TruePosition(2, 4), TruePosition(2, 7));
  const GridPositions& positions = test_line.get_grid_positions();
  EXPECT_FALSE(positions.get_position(0, 0));
  EXPECT_FALSE(positions.get_position(0, 1));
  EXPECT_FALSE(positions.get_position(0, 2));
  EXPECT_FALSE(positions.get_position(0, 3));
  EXPECT_FALSE(positions.get_position(0, 4));

  EXPECT_TRUE(positions.get_position(2, 4));
  EXPECT_TRUE(positions.get_position(2, 5));
  EXPECT_TRUE(positions.get_position(2, 6));
  EXPECT_TRUE(positions.get_position(2, 7));
}

TEST(Object, move_object)
{
  Line test_line(TruePosition(0, 1), TruePosition(0, 2));
  const GridPositions& positions = test_line.get_grid_positions();
  EXPECT_FALSE(positions.get_position(0, 0));
  EXPECT_TRUE(positions.get_position(0, 1));
  EXPECT_TRUE(positions.get_position(0, 2));
  EXPECT_FALSE(positions.get_position(0, 3));

  test_line.move_object(TruePosition(1, 0));
  EXPECT_FALSE(positions.get_position(1, 0));
  EXPECT_TRUE(positions.get_position(1, 1));
  EXPECT_TRUE(positions.get_position(1, 2));
  EXPECT_FALSE(positions.get_position(1, 3));

}