#include "gtest/gtest.h"

#include "positions.hpp"

TEST(Positions, add_postion)
{
  Positions<int> pos;
  pos.add_position(Vector<int>(0, 0));
  pos.add_position(Vector<int>{0, 1});
  //EXPECT_ANY_THROW(pos.add_position(Vector<int>(0, 1)));
  EXPECT_TRUE(pos.get_position(Vector<int>{0, 0}));
  EXPECT_TRUE(pos.get_position(Vector<int>{0, 0}));
  EXPECT_TRUE(pos.get_position(Vector<int>{0, 1}));
  EXPECT_TRUE(pos.get_position(Vector<int>{0, 1}));
  EXPECT_FALSE(pos.get_position(Vector<int>{1, 1}));
  EXPECT_FALSE(pos.get_position(Vector<int>{1, 1}));
}

/*
TEST(Positions, remove_postion)
{
  Positions pos;
  pos.add_position(Vector{0, 0});
  pos.add_position(Vector{0, 1});
  EXPECT_TRUE(pos.get_position(Vector{0, 0}));
  EXPECT_TRUE(pos.get_position(Vector{0, 1}));
  EXPECT_TRUE(pos.remove_position(Vector{0, 1}));
  EXPECT_FALSE(pos.get_position(Vector{0, 1}));
  EXPECT_ANY_THROW(pos.remove_position(Vector{0, 1}));
  EXPECT_TRUE(pos.get_position(Vector{0, 0}));

  pos.add_position(Vector{0, 1});
  EXPECT_TRUE(pos.get_position(Vector{0, 1}));
}

TEST(Positions, get_coinciding_positions)
{
  Positions first;
  first.add_position(Vector{0, 0});
  first.add_position(Vector{0, 1});
  first.add_position(Vector{1, 1});

  Positions second;
  second.add_position(Vector{0, 0});
  second.add_position(Vector{0, 1});

  Positions coinciding = first.get_coinciding_positions(second);
  ASSERT_TRUE(coinciding.get_position(Vector{0, 0}));
  ASSERT_TRUE(coinciding.get_position(Vector{0, 1}));
  ASSERT_TRUE(coinciding.remove_position(Vector{0, 0}));
  ASSERT_TRUE(coinciding.remove_position(Vector{0, 1}));
  ASSERT_TRUE(coinciding.empty());
}

TEST(Positions, empty)
{
  Positions pos;
  EXPECT_TRUE(pos.empty());
  pos.add_position(Vector{0, 0});
  pos.add_position(Vector{0, 1});
  EXPECT_FALSE(pos.empty());
  pos.remove_position(Vector{0, 1});
  EXPECT_FALSE(pos.empty());
  pos.remove_position(Vector{0, 0});
  EXPECT_TRUE(pos.empty());
}*/
