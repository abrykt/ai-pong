#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "line.hpp"
#include "ball.hpp"

/*
  |
  | X           X
  |  X         X
  |   X       X
  |    X     X
  |     X   X
  |      X X
  |       X
  ------------------
 */

TEST(line, angle_after_bounch)
{
  Line l(TruePosition(0, 0), TruePosition(10, 0));
  Ball r(TruePosition(1 , 1), Velocity(1, -1));

  Velocity new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(1, 1);

  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

TEST(line, angle_after_bounch_line_reversed)
{
  Line l(TruePosition(10, 0), TruePosition(0, 0));
  Ball r(TruePosition(1 , 1), Velocity(1, -1));

  auto new_velocity = l.get_velocity_after_bounce(r);
  TruePosition normalized_velocity(1, 1);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

/*
|
|
+----------------+
|      X
|     X X
|    X   X
|   X     X
|  X       X
|

 */

TEST(line, angle_reversed_after_bounch)
{
  Line l(TruePosition(0, 3), TruePosition(10, 3));
  Ball r(TruePosition(2 , 2), Velocity(-1, 1));

  Velocity new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(-1, -1);

  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

TEST(line, angle_reversed_after_bounch_line_reversed)
{

  Line l(TruePosition(10, 3), TruePosition(0, 3));
  Ball r(TruePosition(2 , 2), Velocity(-1, 1));

  auto new_velocity = l.get_velocity_after_bounce(r);
  TruePosition normalized_velocity(-1, -1);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

/*
     +
     |
X    |
 X   |
  X  |
   X |
    X|
   X |
  X  |
 X   |
X    |
     |
     +
 */

TEST(line, angle_after_bounch_vline)
{

  Line l(TruePosition(4, 0), TruePosition(4, 10));
  Ball r(TruePosition(3 , 3), Velocity(1, 1));

  auto new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(-1, 1);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

TEST(line, angle_after_bounch_vline_reversed)
{

  Line l(TruePosition(4, 10), TruePosition(4, 0));
  Ball r(TruePosition(3 , 3), Velocity(1, 1));

  auto new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(-1, 1);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

TEST(line, angle_reversed_after_bounch_vline)
{

  Line l(TruePosition(4, 0), TruePosition(4, 10));
  Ball r(TruePosition(3 , 3), Velocity(1, -1));

  auto new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(-1, -1);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

TEST(line, angle_reversed_after_bounch_vline_reversed)
{

  Line l(TruePosition(4, 10), TruePosition(4, 0));
  Ball r(TruePosition(3 , 3), Velocity(1, -1));

  auto new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(-1, -1);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

// TODO Add more test with hit on left side wall.
/*
+
|
|
|
+----------+
|
|
|
+
 */

TEST(line, perpendicular_hit_from_right_vline)
{

  Line l(TruePosition(4, 0), TruePosition(4, 10));
  Ball r(TruePosition(5 , 5), Velocity(-1, 0));

  auto new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(1, 0);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

TEST(line, perpendicular_hit_from_right_vline_reversed)
{

  Line l(TruePosition(4, 10), TruePosition(4, 0));
  Ball r(TruePosition(5 , 5), Velocity(-1, 0));

  auto new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(1, 0);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

/*
+
           |
           |
           |
+----------+
           |
           |
           |
 */

TEST(line, perpendicular_hit_from_left_vline)
{

  Line l(TruePosition(4, 0), TruePosition(4, 10));
  Ball r(TruePosition(3 , 3), Velocity(1, 0));

  auto new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(-1, 0);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}

TEST(line, perpendicular_hit_from_left_vline_reversed)
{

  Line l(TruePosition(4, 10), TruePosition(4, 0));
  Ball r(TruePosition(3 , 3), Velocity(1, 0));

  auto new_velocity = l.get_velocity_after_bounce(r);
  auto normalized_velocity = TruePosition(-1, 0);
  EXPECT_DOUBLE_EQ(normalized_velocity[0], new_velocity[0]);
  EXPECT_DOUBLE_EQ(normalized_velocity[1], new_velocity[1]);
}