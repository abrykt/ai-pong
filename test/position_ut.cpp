#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "position.hpp"

// TODO Should update this with more tests for both int and double.

TEST(Position, are_same)
{
  EXPECT_TRUE(GridPosition(1, 0) ==(GridPosition(1, 0)));
  EXPECT_TRUE(GridPosition(0, 0) == (GridPosition(0, 0)));

  EXPECT_FALSE(GridPosition(1, 0) == (GridPosition(0, 1)));
  EXPECT_FALSE(GridPosition(1, 0) == (GridPosition(0, 0)));
}

TEST(Position, operator_plus)
{
  GridPosition sum = GridPosition(1, 0) + GridPosition(0, 1);
  EXPECT_EQ(GridPosition(1, 1), sum);
}

TEST(Position, operator_minus)
{
  GridPosition sum = GridPosition(2, 1) - GridPosition(3, 3);
  EXPECT_EQ(GridPosition(-1, -2), sum);
}

TEST(Position, operator_plus_equal)
{
  TruePosition first(1.1, 2.1);
  TruePosition second(3.2, 0.1);
  first += second;
  EXPECT_THAT(TruePosition(4.3, 2.2) - first, testing::Each(testing::Lt(0.0000000000000000000001)));
}

TEST(Position, flip)
{
  TruePosition pos(-1, 2);
  EXPECT_EQ(TruePosition(2, -1), pos.shift());
  EXPECT_EQ(TruePosition(2, -1), pos);
}

TEST(Position, operator_equality)
{
  EXPECT_FALSE(TruePosition(1.1, 2.1) == TruePosition(3.2, 0.1));
  EXPECT_TRUE(TruePosition(1.1, 2.1) == TruePosition(1.1, 2.1));
}