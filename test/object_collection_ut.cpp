#include <memory>
#include <racket.hpp>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "object_collection.hpp"
#include "line.hpp"
#include "ball.hpp"
#include "pong_game.hpp"
#include "contender_wall.hpp"
#include "object_collection_event_mock.hpp"

class ObjectCollectionTest : public ::testing::Test
{
 public:
  ObjectCollectionTest() : collection_(std::make_shared<ObjectCollection>(object_collection_event_))
  {
  }

  ObjectCollectionEventMock object_collection_event_;
  std::shared_ptr<ObjectCollection> collection_;
};

TEST_F(ObjectCollectionTest, hit)
{
  auto ball = std::make_shared<Ball>(TruePosition(5, 1.1), Velocity(1, -1.000001), collection_);
  collection_->add_object(ball);

  auto line = std::make_shared<Line>(TruePosition(4, 1), TruePosition(8, 1), Velocity(0, 0), collection_);
  collection_->add_object(line);

  //auto third = std::make_shared<Line>(TruePosition(0, 3), TruePosition(0, 4), Velocity(0, 0), collection_);
  //collection_->add_object(third);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(1);

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);

  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), line.get());
}

TEST_F(ObjectCollectionTest, hit_high_speed)
{
  auto ball = std::make_shared<Ball>(TruePosition(5, 1.1), Velocity(10, -10.00000001), collection_);
  collection_->add_object(ball);

  auto line = std::make_shared<Line>(TruePosition(4, 1), TruePosition(8, 1), Velocity(0, 0), collection_);
  collection_->add_object(line);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(1);

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);

  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), line.get());
}

TEST_F(ObjectCollectionTest, hit2)
{
  auto ball = std::make_shared<Ball>(TruePosition(49.5, 49.5), Velocity(-1, -1.000001), collection_);
  collection_->add_object(ball);

  auto line = std::make_shared<Line>(TruePosition(40, 49), TruePosition(80, 49), collection_);
  collection_->add_object(line);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(1);

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);

  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), line.get());
}

TEST_F(ObjectCollectionTest, hit2_high_speed)
{
  auto ball = std::make_shared<Ball>(TruePosition(49.5, 49.5), Velocity(-5, -5.00001), collection_);
  collection_->add_object(ball);

  auto line = std::make_shared<Line>(TruePosition(40, 49), TruePosition(80, 49), collection_);
  collection_->add_object(line);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(1);

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);

  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), line.get());
}

TEST_F(ObjectCollectionTest, hit3)
{
  auto ball = std::make_shared<Ball>(TruePosition(49.5, 49.5), Velocity(1, 1.0001), collection_);
  collection_->add_object(ball);

  auto line = std::make_shared<Line>(TruePosition(40, 50), TruePosition(80, 50), collection_);
  collection_->add_object(line);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(1);

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);

  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), line.get());
}

TEST_F(ObjectCollectionTest, hit3_high_speed)
{
  auto ball = std::make_shared<Ball>(TruePosition(49.5, 49.5), Velocity(5, 5.00001), collection_);
  collection_->add_object(ball);

  auto line = std::make_shared<Line>(TruePosition(40, 50), TruePosition(80, 50), collection_);
  collection_->add_object(line);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(1);

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);

  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), line.get());
}


TEST_F(ObjectCollectionTest, racket_hit_vertical_line)
{
  auto racket_start = TruePosition(94.5, 99.5);
  auto racket_end = TruePosition(99.6, 99.5);

  auto racket = std::make_shared<Racket>(racket_start, racket_end, collection_);
  collection_->add_object(racket);

  auto line = std::make_shared<Line>(TruePosition(100, 0), TruePosition(100, 100), collection_);
  collection_->add_object(line);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(1);

  racket->increase_speed_right();

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*racket, output);
  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), line.get());

  auto position = racket->get_edges();
  EXPECT_EQ(racket_start.at(0), position.at(0).first.at(0));
  EXPECT_EQ(racket_start.at(1), position.at(0).first.at(1));

  EXPECT_EQ(racket_end.at(0), position.at(0).second.at(0));
  EXPECT_EQ(racket_end.at(1), position.at(0).second.at(1));
}

TEST_F(ObjectCollectionTest, ball_update)
{
  auto line = std::make_shared<Line>(TruePosition(3, 1), TruePosition(3, 3), Velocity(0, 0), collection_);
  collection_->add_object(line);

  auto ball = std::make_shared<Ball>(TruePosition(2, 2), Velocity(2.5, 0), collection_);
  collection_->add_object(ball);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(::testing::AtLeast(1));

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);
  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), line.get());

  collection_->update_all_objects();
// TODO
//  auto edges = ball->get_edges();
//  auto start = edges[0].first;
//  auto end = edges[0].second;
//  EXPECT_NEAR(1.5, start[0], 1 - ALMOST_ALL_THE_WAY);
//  EXPECT_DOUBLE_EQ(2, start[1]);
//  EXPECT_NEAR(1.5, end[0], 1.0 - ALMOST_ALL_THE_WAY);
//  EXPECT_DOUBLE_EQ(2, end[1]);
}

TEST_F(ObjectCollectionTest, ball_update_when_in_corner)
{
  auto v_line = std::make_shared<Line>(TruePosition(3, 0), TruePosition(3, 3), collection_);
  collection_->add_object(v_line);

  auto h_line = std::make_shared<ContenderWall>(TruePosition(2, 1), TruePosition(4, 1), collection_,
                                                ObjectType::PLAYER_WALL);
  collection_->add_object(h_line);

  auto ball = std::make_shared<Ball>(TruePosition(2.5, 2.5), Velocity(1, -1.000000000001), collection_);
  collection_->add_object(ball);

  EXPECT_CALL(object_collection_event_, DefaultObjectHit()).Times(::testing::AtLeast(1));

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);
  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), v_line.get());

  collection_->update_all_objects();

// TODO
//  auto edges = ball->get_edges();
//  auto start = edges[0].first;
//  auto end = edges[0].second;
//  EXPECT_NEAR(2.5, start[0], 1 - ALMOST_ALL_THE_WAY);
//  EXPECT_NEAR(1.5, start[1], 1 - ALMOST_ALL_THE_WAY);
//  EXPECT_NEAR(2.5, end[0], 1 - ALMOST_ALL_THE_WAY);
//  EXPECT_NEAR(1.5, end[1], 1 - ALMOST_ALL_THE_WAY);
}

TEST_F(ObjectCollectionTest, ball_update_when_in_corner2)
{
  auto v_line = std::make_shared<Line>(TruePosition(3, 0), TruePosition(3, 3), collection_);
  collection_->add_object(v_line);

  auto h_line = std::make_shared<ContenderWall>(TruePosition(2, 1), TruePosition(4, 1), collection_,
                                                ObjectType::PLAYER_WALL);
  collection_->add_object(h_line);

  auto ball = std::make_shared<Ball>(TruePosition(3, 1.5), Velocity(0, -1.0), collection_);
  collection_->add_object(ball);

  EXPECT_CALL(object_collection_event_, OnPlayerWallHit()).Times(::testing::AtLeast(1));

  HitObjects output;
  bool found_a_hit = collection_->get_hit_objects(*ball, output);
  EXPECT_TRUE(found_a_hit);
  EXPECT_EQ(output.at(0).get(), h_line.get());

  collection_->update_all_objects();
}

TEST_F(ObjectCollectionTest, ball_update_in_corner3)
{
  int x_size = 20;
  int y_size = 20;
  collection_->add_object(std::make_shared<Line>(TruePosition(x_size - 1, 0 - MOVING_OBJECT_OFFSET),
                                                 TruePosition(x_size - 1, y_size - 1 + MOVING_OBJECT_OFFSET),
                                                 collection_));

  collection_->add_object(std::make_shared<ContenderWall>(TruePosition(0 - MOVING_OBJECT_OFFSET, 0),
                                                          TruePosition(x_size - 1 + MOVING_OBJECT_OFFSET, 0),
                                                          collection_,
                                                          ObjectType::OPPONENT_WALL));

  double xy = 18.007999999999999;
  auto ball = std::make_shared<Ball>(TruePosition(xy, xy + 0.00001), Velocity(1.001, 1.00001), collection_);
  collection_->add_object(ball);

  // TODO EXPECT_CALL(*object_collection_event_, OnOpponentWallHit()).Times(1);
  // TODO EXPECT_CALL(*object_collection_event_, AnyObjectHit()).Times(1);
  collection_->update_all_objects();
}
