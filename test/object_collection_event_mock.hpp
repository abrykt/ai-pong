#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <object_collection_events_i.hpp>

class ObjectCollectionEventMock : public ObjectCollectionEventI
{
 public:
  MOCK_METHOD0(OnPlayerWallHit, void());
  MOCK_METHOD0(OnOpponentWallHit, void());
  MOCK_METHOD0(OnPlayerRacketHit, void());
  MOCK_METHOD0(OnOpponentRacketHit, void());
  MOCK_METHOD0(DefaultObjectHit, void());
};