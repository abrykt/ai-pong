#include <iostream>
#include "gtest/gtest.h"

#include "tools/random.hpp"
#include "pong_game.hpp"
#include "game_board.hpp"
#include "pong_game_factory.hpp"



void run_game()
{
  unsigned x_size = 1000;
  unsigned y_size = 1000;
  unsigned point_size = 10;
  PongGamePtr pg = PongGameFactory::create_pong_game(x_size / point_size, y_size / point_size);

  unsigned num_moves = 1000;
  std::shared_ptr<GameBoard> board = std::make_shared<GameBoard>(x_size / point_size, y_size / point_size);
  for (unsigned i = 0; i < num_moves; ++i)
  {
    pg->draw_current_board(board);
    board->clear_board();
    pg->move_racket(tools::get_random_int(0, 1) ? Direction::Left : Direction::Rigth);
    pg->update();
  }
}


TEST(pong, test_no_throw)
{
  EXPECT_NO_THROW(run_game());
}

TEST(pong, test_not_fatal)
{
  EXPECT_NO_FATAL_FAILURE(run_game());
}

TEST(pong, move_racket_to_end)
{
  unsigned x_size = 100;
  unsigned y_size = 100;
  PongGamePtr pg = PongGameFactory::create_pong_game(x_size, y_size);

  unsigned num_moves = 100;
  std::shared_ptr<GameBoard> board = std::make_shared<GameBoard>(x_size, y_size);
  for (unsigned i = 0; i < num_moves; ++i)
  {
    pg->draw_current_board(board);
    board->clear_board();
    pg->move_racket(Direction::Left);
    pg->update();
  }
}