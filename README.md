### C++ AI pong ###

This repository contains a C++ version of Andrej Karpathys *Deep Reinforcement Learning: Pong from Pixels*.
Information about the algorithm can be found here: http://karpathy.github.io/2016/05/31/rl/

This project was done as a part of the course "Introduction to artificial intelligence for game programming"
 at Uppsala University.

### How do I get set up? ###

You need SDL2, GL and Cmake.

* Install dependencies with

```apt-get install libsdl2-dev libgl-dev cmake```


### To build ###

```cd pong```

```mkdir build```

```cd build```

```cmake ..```

```make```

Three binaries are produced.

* "learner"

Application that play pong games. One player is  a simple player that just follows the ball with a certain speed.
The other player learns how to play in an optimal way by looking at the outcome of decisions and then updating weight parameters
to make decisions that favors wins.

* "pong"

A pong game where you can control one of the players.

* "unittests"

Some unittests written in gtest.

### Contact inforamtion ###

* Feel free to contact me at abrykt@gmail.com.