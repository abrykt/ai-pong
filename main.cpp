
#ifndef WIN32
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif


#include <chrono>
#include <thread>
#include <pong_game.hpp>

#include "tools/gui.hpp"
#include "tools/gui_helpers.hpp"
#include "tools/random.hpp"

#include "pong_game_factory.hpp"
#include "pong.hpp"
#include "game_board.hpp"
#include "pong_game_factory.hpp"


#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic push

bool handle_event(SDL_Event& event, PongGameI& pg)
{
  int key;//, state;
  bool quit = false;
  switch (event.type)
  {
    case SDL_KEYDOWN:
      key = event.key.keysym.sym;
      //state = SDL_GetModState();

      if (key == SDLK_ESCAPE)
      {
        quit = true;
        break;
      }
      if (key < 128)
      {
        //G.NormalKeys(key, state);
      }
      else
      {
        if(key == SDLK_LEFT)
        {
          pg.move_racket(Direction::Left);
        }
        if(key == SDLK_RIGHT)
        {
          pg.move_racket(Direction::Rigth);
        }
      }
      break;
    case SDL_QUIT:
    {
      quit = true;
      break;
    }
    default:
    {
      break;
    }
  }
  return quit;
}

int main(int argc, char* argv[])
{
  unsigned x_size = 800;
  unsigned y_size = 800;
  unsigned point_size = 10;
  PongGamePtr pg = PongGameFactory::create_pong_game(x_size / point_size, y_size / point_size);

  tools::Gui gui(x_size, y_size, tools::White);
  tools::PixelMatrix pixel_matrix;

  std::shared_ptr<GameBoard> board = std::make_shared<GameBoard>(x_size / point_size, y_size / point_size);

  SDL_Event event;
  bool quit = false;

  while (!quit)
  {
    if(!pg->get_game_over_state())
    {
      pg->draw_current_board(board);
      tools::bool_to_color_matrix(board->get_board(), pixel_matrix);
      board->clear_board();
      gui.Clear();
      gui.draw_matrix(pixel_matrix, tools::PixelPoint(point_size / 2, point_size / 2), point_size);
      gui.Render();
      while (!quit && SDL_PollEvent(&event))
      {
        quit = handle_event(event, *pg);
      }
      pg->update();
      SDL_Delay(30);
    }
    else
    {
      switch (pg->get_winner())
      {
        case Winner::You:
        {
          std::cout << "You won!" << std::endl;
          break;
        }
        case Winner::Opponenet:
        {
          std::cout << "You lost!" << std::endl;
          break;
        }
        default:
        {
          assert("No winner declared. Something is wrong...");
          break;
        }
      }
      pg->clear_game();
    }
  }


}

#pragma GCC diagnostic pop
#pragma GCC diagnostic ignored "-Wuninitialized"