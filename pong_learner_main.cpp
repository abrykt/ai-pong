//
// Created by brykt on 2017-12-08.
//

#include "tools/stl_helpers.hpp"
#include "tools/log.hpp"
#include "tools/measure_time.hpp"
#include "pong_learner.hpp"

int main(int argc, char* argv[])
{
  std::cout << "Pass '-noui' to disable showing games progress." << std::endl;
  std::cout << "Please tail 'application.log' for progress information." << std::endl;
  ShowProgressInUi showprogress = ShowProgressInUi::yes;
  if(argc > 1)
  {
    if (!strcmp("-noui", argv[1]))
    {
      showprogress = ShowProgressInUi::no;
    }
  }

  PongLearner pong_learner;
  LOG_INFO("Starting pong learner");

  auto f = std::bind(&PongLearner::learn_pong_and_show_games, &pong_learner, showprogress);
  std::cout << "execution took " << tools::measure<>::execution<>(f) << "ms." << std::endl;

  return 0;
}
