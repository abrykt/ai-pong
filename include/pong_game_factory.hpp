#pragma once

#include <memory>

class PongGameI;

typedef std::unique_ptr<PongGameI> PongGamePtr;

class PongGameFactory
{
 public:
  static std::unique_ptr<PongGameI> create_pong_game(unsigned x_size, unsigned y_size);
};

