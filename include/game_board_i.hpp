#pragma once


template<typename T>
class Positions;

class GameBoardI
{
 public:
  virtual ~GameBoardI()
  {

  }

  virtual void set(const Positions<int>& positions) = 0;
  virtual void clear_board() = 0;
};
