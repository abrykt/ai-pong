#pragma once

#include <memory>

template<typename T>
class Positions;
class GameBoardI;

enum class Direction
{
  Left,
  Rigth
};

enum class Winner
{
  None,
  You,
  Opponenet
};

class PongGameI
{
 public:
  virtual ~PongGameI() { }
  virtual void move_racket(Direction direction) = 0;
  virtual void update() = 0;
  virtual void draw_current_board(std::shared_ptr<GameBoardI> board) const = 0;
  virtual void clear_game() = 0;
  virtual bool get_game_over_state() const = 0;
  virtual Winner get_winner() const = 0;
};