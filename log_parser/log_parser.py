from matplotlib import pyplot as plt
import numpy as np
import re
import unittest
import argparse


def parse_line(line):
    # Example line
    # Mon Jan  8 14:27:47:454 13183279267505002241 INFO    pong_learner.hpp:100             learn_pong                                rounds played 10, won since last update 33, lost 210

    pattern = r'(.*)rounds played (.*), won since last update (.*), lost (.*)'
    matchObj = re.match(pattern, line)
    if matchObj:
        rounds_played = matchObj.group(2)
        won = matchObj.group(3)
        lost = matchObj.group(4)
        return rounds_played, won, lost
    else:
        return False, False, False


def parse_log(lines):
    all_num_values_seen = []
    all_won = []
    all_lost = []
    list_of_lines = lines.splitlines()
    for line in list_of_lines:
        num_values_seen, won, lost = parse_line(line)
        if num_values_seen:
            all_num_values_seen.append(float(num_values_seen))
            all_won.append(float(won))
            all_lost.append(float(lost))

    return all_num_values_seen, all_won, all_lost


def running_mean(values):
    running_means = np.zeros(shape=(len(values) + 1, 1))
    index = 1
    for value in values:
        running_means[index] = running_means[index - 1] * (index - 1) / index + value / index
        index += 1
    return running_means[1:]


def running_mean_x(register, X):
    register_mean = [np.mean(register[max(i - X, 0): max(i, 0) + 1]) for i in range(len(register))]
    return register_mean

def plot_log(filename):
    with open(filename) as f:
        lines = f.read()
        all_num_values_seen, all_won, all_lost = parse_log(lines)
        all_won_np = np.array(all_won)
        all_lost_np = np.array(all_lost)
        total = all_won_np + all_lost_np
        ratio = np.divide(all_won,  total)

        plt.subplot(211)
        plt.plot(all_num_values_seen,  ratio)
        plt.title("Ratio between wins and lost.")
        plt.ylabel("Ratio")

        rm = running_mean_x(ratio, X=100)

        plt.subplot(212)
        plt.plot(all_num_values_seen, rm)
        plt.title("Mean of ratio for 100 last games.")
        plt.xlabel("Round")
        plt.ylabel("Running mean of ratio.")
        plt.tight_layout()

        plt.savefig("learning_performance.pdf", format='pdf')
        plt.show()


class TestParseLog(unittest.TestCase):

    def setUp(self):
        pass

    def test_parse_line(self):
        line = 'Mon Jan  8 14:27:47:454 13183279267505002241 INFO    pong_learner.hpp:100             learn_pong                                rounds played 10, won since last update 33, lost 210'
        rounds_played, won, lost = parse_line(line)
        self.assertEqual('10', rounds_played)
        self.assertEqual('33', won)
        self.assertEqual('210', lost)

    def _test_parse_many_loglines(self):
        lines = '''
Logger created.
Mon Jan  8 14:27:46:160 13183279267505002241 INFO    pong_learner_main.cpp:15         main                                      Starting pong learner
Mon Jan  8 14:27:47:454 13183279267505002241 INFO    pong_learner.hpp:100             learn_pong                                rounds played 10, won since last update 33, lost 210
Mon Jan  8 14:28:00:201 13183279267505002241 INFO    rms_prop_gradient_applier.hpp:38    apply_gradients                           Applying gradients.
Mon Jan  8 14:28:01:494 13183279267505002241 INFO    pong_learner.hpp:100             learn_pong                                rounds played 20, won since last update 38, lost 210
Mon Jan  8 14:28:18:253 13183279267505002241 INFO    rms_prop_gradient_applier.hpp:38    apply_gradients                           Applying gradients.
Mon Jan  8 14:28:19:405 13183279267505002241 INFO    pong_learner.hpp:100             learn_pong                                rounds played 30, won since last update 35, lost 210
Mon Jan  8 14:28:27:48  13183279267505002241 INFO    rms_prop_gradient_applier.hpp:38    apply_gradients                           Applying gradients.
Mon Jan  8 14:28:28:214 13183279267505002241 INFO    pong_learner.hpp:100             learn_pong                                rounds played 40, won since last update 26, lost 210
Mon Jan  8 14:28:40:762 13183279267505002241 INFO    rms_prop_gradient_applier.hpp:38    apply_gradients                           Applying gradients.'''
        parse_log(lines)

    def test_read_file(self):
        filename = 'application.log'
        plot_log(filename=filename)

    def test_running_mean(self):
        values = [1, 2, 3, 4, 5]
        rm = running_mean(values)
        print(rm)

    def test_plot(self):
        plt.subplot(211)
        X = np.linspace(-np.pi, np.pi, 256, endpoint=True)
        C, S = np.cos(X), np.sin(X)

        plt.plot(X, C)
        plt.subplot(212)
        plt.plot(X, S)
        plt.show()

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', action='store', dest='filename', type=str)
    args = parser.parse_args()
    filename = args.filename
    if filename:
        plot_log(filename=filename)
    else:
        unittest.main()
