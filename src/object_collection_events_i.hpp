#pragma once

class ObjectCollectionEventI
{
 public:
  virtual void OnPlayerWallHit() = 0;
  virtual void OnOpponentWallHit() = 0;
  virtual void OnPlayerRacketHit() = 0;
  virtual void OnOpponentRacketHit() = 0;
  virtual void DefaultObjectHit() = 0;

};