#pragma once

#include <cassert>
#include <limits>

#include "tools/stl_helpers.hpp"

#include "positions.hpp"
#include "object_collection.hpp"
#include "object_i.hpp"
#include "game_board.hpp"


class Object : public ObjectI
{
 public:
  explicit Object() :
          velocity_(Velocity(0, 0))
  {

  }

  explicit Object(const Velocity& velocity) :
          velocity_(velocity)
  {

  }

  explicit Object(std::weak_ptr<ObjectCollectionI> object_collection) :
          velocity_(Velocity(0, 0)),
          object_collection_(std::move(object_collection))
  {

  }

  explicit Object(const Velocity& velocity,
                  std::weak_ptr<ObjectCollectionI> object_collection) :
          velocity_(velocity),
          object_collection_(std::move(object_collection))
  {

  }

  virtual ~Object() = default;

  DISABLE_COPY_ASSIGN_MOVE(Object);

  virtual void set_velocity(const Velocity& velocity)
  {
    using tools::are_same;
    double const zero = 0;
    if (are_same(velocity[0], velocity[1]) && !are_same(velocity[0], zero))
    {
      // Something gets meesed up when velocity is equal and we hit multiple objects at the same hit.
      // Need to investigate more.
      throw std::invalid_argument("Velocity cannot have equal not zero components.");
    }
    velocity_ = velocity;
  }

  virtual Velocity get_velocity() const
  {
    return velocity_;
  }

  virtual void update()
  {
    if (!(get_velocity() == Velocity(0, 0)))
    {
      Velocity v = get_velocity();
      move_object(v);
    }
  }

  const GridPositions& get_grid_positions() const
  {
    return grid_positions_;
  }

  const TruePositions& get_true_positions() const
  {
    return true_positions_;
  }

  void draw(std::shared_ptr<GameBoardI> game_board)
  {
    game_board->set(get_grid_positions());
  }

  bool is_hit(const ObjectI& other) const
  {
    TruePosition temp_pos;
    return is_hit(other, temp_pos);
  }

  bool is_hit(const ObjectI& other, TruePosition& movement_to_hit) const
  {
    auto my_edge = get_edges()[0];
    auto velocity = get_velocity();
    auto my_next_start = my_edge.first + velocity;
    auto my_next_end = my_edge.second + velocity;

    auto others_edge = other.get_edges()[0];
    auto other_velocity = other.get_velocity();

    TruePosition closest_hit_position = TruePosition(std::numeric_limits<double>::max(),
                                                     std::numeric_limits<double>::max());
    double smallest_distance_to_hit;

    auto will_point_cross_line = [&](const TruePosition& point, const Velocity& velocity,
                                     const TruePosition& line_start, const TruePosition line_end)
    {
      TruePosition hit_coordinates;
      bool hit = tools::get_line_intersection(point, point + velocity, line_start, line_end, hit_coordinates);
      smallest_distance_to_hit = tools::point_distance(point.to_tuple(), closest_hit_position.to_tuple());
      auto distance_to_this_hit = tools::point_distance(point.to_tuple(), hit_coordinates.to_tuple());
      if (distance_to_this_hit < smallest_distance_to_hit)
      {
        closest_hit_position = hit_coordinates;
        movement_to_hit = hit_coordinates - point;
      }
      return hit;
    };

    bool hit_first = will_point_cross_line(others_edge.first, other_velocity, my_next_start, my_next_end);
    bool hit_second = will_point_cross_line(others_edge.second, other_velocity, my_next_start, my_next_end);
    return hit_first || hit_second;

  }

  bool is_hit_(const ObjectI& other) const
  {
    auto my_positions = get_grid_positions();
    TruePositions other_positions = other.get_true_positions();
    other_positions.move_positions(other.get_velocity());

    GridPositions other_new_grid_position = trues_to_grids(other_positions);

    auto coinciding_positions = other_new_grid_position.get_coinciding_positions(my_positions);
    bool empty = coinciding_positions.empty();
    return !empty;
  }

  virtual Angle get_hit_angle(const ObjectI& object) const
  {
    TAG_UNUSED(object);
    assert(false);
    return Angle();
  }

  Velocity get_velocity_after_bounce(const ObjectI& object) const
  {
    TAG_UNUSED(object);
    assert(false);
    return Velocity();
  }

  virtual ObjectType get_object_type() const
  {
    return ObjectType::DEFAULT;
  }

  void update_velocity_and_position_after_hit_with_object(std::shared_ptr<ObjectI> object)
  {
    TAG_UNUSED(object);
    assert(false);
  }


 protected:
  void add_position(const GridPosition& position)
  {
    grid_positions_.add_position(position);
  }

  void add_position(const TruePosition& true_position)
  {
    if (grid_positions_.add_position(true_to_grid(true_position)))
    {
      true_positions_.add_position(true_position);
    }
  }

  void clear_positions()
  {
    grid_positions_.clear();
    true_positions_.clear();
  }

  std::weak_ptr<ObjectCollectionI> get_object_collection() const
  {
    return object_collection_;
  }

 private:

  GridPositions grid_positions_;
  TruePositions true_positions_;
  Velocity velocity_;
  std::weak_ptr<ObjectCollectionI> object_collection_;

};