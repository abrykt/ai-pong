#pragma once

#include <memory>

#include <tools/gui.hpp>
#include <tools/gui_helpers.hpp>
#include "pong_player.hpp"

class GameVisualizer
{
 public:
  GameVisualizer(unsigned num_rows, unsigned num_cols) :
          gui_(num_cols * 10, num_rows * 10, tools::White),
          pixel_matrix_(num_rows, std::vector<tools::Color>(num_cols)
          )
  {
    point_size_ = 10.0;
  }

  void show_game(const PongPlayer::TrainingDataType& game_data)
  {
    for (auto&& game_datum : game_data)
    {
      tools::eigen_vector_bool_to_color_matrix(*game_datum, pixel_matrix_);

      gui_.Clear();
      gui_.draw_matrix(pixel_matrix_, tools::PixelPoint(point_size_ / 2, point_size_ / 2), point_size_);
      gui_.Render();

      SDL_Delay(20);
    }
  }

 private:
  tools::Gui gui_;
  tools::PixelMatrix pixel_matrix_;
  double point_size_;
};

