#pragma once

#include <vector>
#include <stdexcept>
#include <iostream>

#include "tools/stl_helpers.hpp"

#include "position.hpp"

template<typename T = int>
class Positions
{
 public:
  explicit Positions()
  {

  }

  ~Positions() = default;

  Positions& operator=(const Positions& lhs) = delete;

  bool add_position(const Vector<T>& position)
  {
    if (!get_position(position))
    {
      positions_.push_back(position);
      return true;
    }
    else
    {
      return false;
    }
  }

  void remove_position(const Vector<T>& position)
  {
    auto iter = find_position(position);
    if (iter == positions_.end())
    {
      throw std::out_of_range("Could not find position: " + position.to_string());
    }
    positions_.erase(iter);
  }

  bool get_position(int x, int y) const
  {
    return find_position(Vector<T>(x, y)) != positions_.end();
  }

  bool get_position(const Vector<T>& position) const
  {
    return find_position(position) != positions_.end();
  }

  Positions<T> get_coinciding_positions(const Positions<T>& other) const
  {
    Positions<T> coinciding_positions;
    for (auto& other_position : other.positions_)
    {
      if (get_position(other_position))
      {
        coinciding_positions.add_position(other_position);
      }
    }
    return coinciding_positions;
  }

  bool empty() const
  {
    return positions_.empty();
  }

  void move_positions(const Vector<T>& relative_position)
  {
    std::vector<Vector<T>> new_positions;
    for (auto& item : positions_)
    {
      new_positions.emplace_back(item + relative_position);
    }
    positions_ = new_positions;
  }

  const std::vector<Vector<T>>& get_positions() const
  {
    return positions_;
  }

  void clear()
  {
    positions_.clear();
  }

 private:

  typename std::vector<Vector<T>>::const_iterator find_position(const Vector<T>& position) const
  {
    auto iter = std::find_if(positions_.begin(), positions_.end(), [&](const Vector<T>& p)
    {
      return position == p;
    });
    return iter;
  }

  std::vector<Vector<T>> positions_;

};


typedef Positions<double> TruePositions;
typedef Positions<int> GridPositions;

GridPosition true_to_grid(const TruePosition& true_position);
GridPositions trues_to_grids(const TruePositions& true_postions);