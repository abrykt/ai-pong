#pragma once

#include "line.hpp"
#include "object_i.hpp"

class ContenderWall : public Line
{
 public:
  explicit ContenderWall(const TruePosition& start,
                const TruePosition& end,
                std::shared_ptr<ObjectCollectionI> object_collection,
                ObjectType object_type) :
          Line(start, end, object_collection),
          object_type_(object_type)
  {

  }

  ContenderWall() = delete;
  DISABLE_COPY_ASSIGN_MOVE(ContenderWall);

  virtual ~ContenderWall() = default;


  ObjectType get_object_type() const override
  {
    return object_type_;
  }

 private:
  ObjectType object_type_;
};
