#pragma once

#include <vector>
#include <memory>

#include "tools/geometry.hpp"
#include "tools/stl_helpers.hpp"

#include "object_collection_events_i.hpp"
#include "pong.hpp"

class PongGame;
class Racket;
class GameBoardI;
class ObjectCollection;

#define MOVING_OBJECT_OFFSET 0.5


class PongGame : public PongGameI, public ObjectCollectionEventI
{
 public:

  PongGame(unsigned x_size, unsigned y_size);
  PongGame() = delete;

  DISABLE_COPY_ASSIGN_MOVE(PongGame);

  void move_racket(Direction direction);
  void update();
  void draw_current_board(std::shared_ptr<GameBoardI> board) const;
  void clear_game();
  bool get_game_over_state() const;
  Winner get_winner() const;

 private:
  void OnPlayerWallHit() override;
  void OnOpponentWallHit() override;
  void OnPlayerRacketHit() override;
  void OnOpponentRacketHit() override;
  void DefaultObjectHit() override;
  std::tuple<double, double> get_initial_ball_velocity();

  unsigned x_size_;
  unsigned y_size_;

  std::shared_ptr<Racket> player_racket_;
  std::shared_ptr<Racket> opponent_racket_;
  std::shared_ptr<ObjectCollection> object_collection_;

  bool game_over_;
  bool you_won_;
};