#pragma once

#include <algorithm>
#include <memory>
#include <vector>

#include "tools/stl_helpers.hpp"

#include "object_i.hpp"
#include "object_collection_i.hpp"
#include "object_collection_events_i.hpp"


class ObjectCollection : public ObjectCollectionI
{
 public:

  explicit ObjectCollection(ObjectCollectionEventI& object_collection_events) :
          object_collection_events_(object_collection_events)
  {

  }

  ObjectCollection() = delete;

  DISABLE_COPY_ASSIGN_MOVE(ObjectCollection);

  void add_object(std::shared_ptr<ObjectI> object)
  {
    objects_.push_back(object);
  }

  const std::vector<std::shared_ptr<ObjectI>>& get_objects()
  {
    return objects_;
  }

  void update_all_objects()
  {
    std::for_each(objects_.begin(), objects_.end(), [](std::shared_ptr<ObjectI>& object)
    {
      object->update();
    });
  }

  void draw(std::shared_ptr<GameBoardI> board)
  {
    std::for_each(objects_.begin(), objects_.end(), [&](const std::shared_ptr<ObjectI>& object)
    {
      object->draw(board);
    });
  }

  bool get_hit_objects(const ObjectI& other, HitObjects& hit_objects) const
  {
    typedef std::vector<std::shared_ptr<ObjectI>>::const_iterator ObjectsIter;
    ObjectsIter possible_hit = std::find_if(objects_.begin(), objects_.end(),
                                            [&](const std::shared_ptr<ObjectI>& object)
                                            {
                                              bool is_hit = object->is_hit(other);
                                              bool is_the_same = &other == object.get();
                                              return is_hit && !is_the_same;
                                            });

    if (possible_hit != objects_.end())
    {
      const std::shared_ptr<ObjectI> hit_object = *possible_hit;
      hit_objects.push_back(hit_object);
      fire_object_hit_event(hit_object->get_object_type());
      return true;
    }
    else
    {
      return false;
    }
  }

  bool update_position_and_velocity(const ObjectI& other) const
  {
    assert(false);
    TAG_UNUSED(other);
    return true;
  }


  bool find_first_object_that_was_hit(const ObjectI& other, std::shared_ptr<ObjectI>& hit_object)
  {
    std::vector<std::pair<std::shared_ptr<ObjectI>, TruePosition>> hit_objects;
    std::for_each(objects_.begin(), objects_.end(), [&](const std::shared_ptr<ObjectI>& object)
    {
      TruePosition movement_to_hit;
      bool is_hit = object->is_hit(other, movement_to_hit);
      bool is_the_same = &other == object.get();
      if (is_hit && !is_the_same)
      {
        hit_objects.emplace_back(std::make_pair(object, movement_to_hit));
      }
    });

    std::sort(hit_objects.begin(), hit_objects.end(), [](const std::pair<std::shared_ptr<ObjectI>, TruePosition>& first,
                                                         const std::pair<std::shared_ptr<ObjectI>, TruePosition>& second)
    {
      return (first.second.norm() < second.second.norm());
    });
    if (!hit_objects.empty())
    {
      hit_object = hit_objects.at(0).first;
      fire_object_hit_event(hit_object->get_object_type());
      return true;
    }
    else
    {
      return false;
    }
  }

 private:

  void fire_object_hit_event(const ObjectType& object_type) const
  {
    switch (object_type)
    {
      case ObjectType::PLAYER_WALL:
      {
        object_collection_events_.OnPlayerWallHit();
        break;
      }
      case ObjectType::OPPONENT_WALL:
      {
        object_collection_events_.OnOpponentWallHit();
        break;
      }
      case ObjectType::PLAYER_RACKET:
      {
        object_collection_events_.OnPlayerRacketHit();
        break;
      }
      case ObjectType::OPPONENT_RACKET:
      {
        object_collection_events_.OnOpponentRacketHit();
        break;
      }
      default:
      {
        object_collection_events_.DefaultObjectHit();
        break;
      }
    }

  }

  std::vector<std::shared_ptr<ObjectI>> objects_;
  ObjectCollectionEventI& object_collection_events_;
};