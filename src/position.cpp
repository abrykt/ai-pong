#include <iostream>

#include "position.hpp"

template<typename K>
std::ostream& operator<<(std::ostream& os, const Vector<K>& position)
{
  os << position.to_string();
  return os;
}

