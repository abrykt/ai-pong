#include <cassert>
#include "game_board.hpp"

GameBoard::GameBoard(unsigned x_size, unsigned y_size) :
        x_size_(x_size),
        y_size_(y_size)
{
  clear_board();
}

void GameBoard::set(const GridPositions& positions)
{
  const std::vector<GridPosition> position_vec = positions.get_positions();
  for (auto&& item : position_vec)
  {
    if (item[1] < 0 || (size_t)item[1] >= board_.size())
    {
      assert(false);
    };
    if (item[0] < 0 || (size_t)item[0] >= board_.at(0).size())
    {
      assert(false);
    }
    board_.at(item[1]).at(item[0]) = true;
  }
}

void GameBoard::clear_board()
{

  board_.clear();
  board_.resize(y_size_);
  for (auto&& row : board_)
  {
    row.clear();
    row.resize(x_size_, false);
  }
}