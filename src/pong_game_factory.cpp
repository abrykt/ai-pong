#include "pong_game_factory.hpp"
#include "pong_game.hpp"

std::unique_ptr<PongGameI> PongGameFactory::create_pong_game(unsigned x_size, unsigned y_size)
{
  return std::unique_ptr<PongGameI>(new PongGame(x_size, y_size));
}

