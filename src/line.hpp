#pragma once

#include <cassert>
#include "object.hpp"
#include "object_collection_i.hpp"


class Line : public Object
{
 public:
  // TODO Look into std::move for below constructor arguments.
  explicit Line(const TruePosition& start,
                const TruePosition& end,
                const Velocity& velocity,
                const std::shared_ptr<ObjectCollectionI>& object_collection) :
          Object(velocity, object_collection),
          start_(start),
          end_(end)
  {
    set_positions();
  }

  explicit Line(const TruePosition& start, const TruePosition& end,
                const std::shared_ptr<ObjectCollectionI>& object_collection) :
          Object(object_collection),
          start_(start),
          end_(end)
  {
    set_positions();
  }

  explicit Line(const TruePosition& position, const std::shared_ptr<ObjectCollectionI>& object_collection) :
          Object(object_collection),
          start_(position),
          end_(position)
  {
    set_positions();
  }

  explicit Line(const TruePosition& start, const TruePosition& end) :
          Object(),
          start_(start),
          end_(end)
  {
    set_positions();
  }

  explicit Line(const TruePosition& start, const TruePosition& end, const Velocity& velocity) :
          Object(velocity),
          start_(start),
          end_(end)
  {
    set_positions();
  }

  DISABLE_COPY_ASSIGN_MOVE(Line);
  Line() = delete;

  virtual ~Line() = default;

  virtual void update()
  {
    Object::update();
  }

  Angle get_hit_angle(const ObjectI& object) const override
  {
    assert(false);
    object.get_grid_positions();
    return Angle(0, 0);
  }

  void move_object(const TruePosition& relative_position)
  {
    // Should fix so tht this takes the update speed into account.
    start_ += relative_position;
    end_ += relative_position;
    set_positions();
  }

  Velocity get_velocity_after_bounce(const ObjectI& other) const
  {
    // http://www.3dkingdoms.com/weekly/weekly.php?a=2

    // First compute the new velocity after bouncing without any impact of this object velocity.
    Angle this_line = (end_ - start_).to_normalized();

    // Find out normal of vector defining the line.
    Velocity v = other.get_velocity();
    Angle normal;
    if (v[1] <= 0) // Ball is travelling down so hit will be on side with normal pointing upwards.
    {
      // (-dy, dx)
      normal = Angle(-this_line[1], this_line[0]);
    }
    else
    {
      // (dy, -dx)
      normal = Angle(this_line[1], -this_line[0]);
    }

    auto k = -2 * (normal[0] * v[0] + normal[1] * v[1]);
    auto r = Angle{k * normal[0] + v[0], k * normal[1] + v[1]};
    r += get_velocity() * 0.9;
    return r;
  }

  ObjectType get_object_type() const override
  {
    return ObjectType::DEFAULT;
  }

  std::vector<std::pair<TruePosition, TruePosition>> get_edges() const
  {
    auto p = std::make_pair(start_, end_);
    return {p};
  }

 private:

  void set_positions()
  {
    clear_positions();
    TruePosition line = end_ - start_;
    //assert(line[0] == 0.0 || line[1] == 0.0); // Only horizontal or vertical line is supported.
    TruePosition to_add = line.to_normalized();
    TruePosition location_to_add = start_;
    do
    {
      add_position(location_to_add);
      location_to_add += to_add;
    } while ((location_to_add.norm() <= end_.norm()));

  }

  TruePosition start_;
  TruePosition end_;
};