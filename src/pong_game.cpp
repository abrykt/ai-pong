#include <cassert>
#include <tools/random.hpp>
#include "pong_game.hpp"
#include "line.hpp"
#include "contender_wall.hpp"
#include "ball.hpp"
#include "racket.hpp"
#include "opponent_racket.hpp"

PongGame::PongGame(unsigned x_size, unsigned y_size) :
        x_size_(x_size),
        y_size_(y_size),
        game_over_(false),
        you_won_(false)
{
  clear_game();
}

void PongGame::draw_current_board(std::shared_ptr<GameBoardI> board) const
{
  object_collection_->draw(board);
}

void PongGame::move_racket(Direction direction)
{
  if (direction == Direction::Left)
  {
    player_racket_->increase_speed_left();
  }
  else
  {
    player_racket_->increase_speed_right();
  }
}

void PongGame::update()
{
  object_collection_->update_all_objects();
}

// private

void PongGame::clear_game()
{
  game_over_ = false;
  you_won_ = false;
  object_collection_ = std::make_shared<ObjectCollection>(*this);

  // TODO Maybe change so that object needs to be created via object
  // collection to make sure that all objects are "tracked."

  // Vertical walls
  object_collection_->add_object(std::make_shared<Line>(TruePosition(0, 0 - MOVING_OBJECT_OFFSET),
                                                        TruePosition(0, y_size_ - 1 + MOVING_OBJECT_OFFSET),
                                                        object_collection_));
  object_collection_->add_object(std::make_shared<Line>(TruePosition(x_size_ - 1, 0 - MOVING_OBJECT_OFFSET),
                                                        TruePosition(x_size_ - 1, y_size_ - 1 + MOVING_OBJECT_OFFSET),
                                                        object_collection_));

  // Horizontal walls
  object_collection_->add_object(std::make_shared<ContenderWall>(TruePosition(0 - MOVING_OBJECT_OFFSET, 0),
                                                                 TruePosition(x_size_ - 1 + MOVING_OBJECT_OFFSET, 0),
                                                                 object_collection_,
                                                                 ObjectType::OPPONENT_WALL));

  object_collection_->add_object(std::make_shared<ContenderWall>(TruePosition(0 - MOVING_OBJECT_OFFSET, y_size_ - 1),
                                                                 TruePosition(x_size_ - 1 + MOVING_OBJECT_OFFSET,
                                                                              y_size_ - 1),
                                                                 object_collection_,
                                                                 ObjectType::PLAYER_WALL));

  auto velocity = get_initial_ball_velocity();
  double x_vel, y_vel;
  std::tie(x_vel, y_vel) = velocity;
  auto ball = std::make_shared<Ball>(TruePosition(x_size_ / 2, y_size_ / 2),
                                     Velocity(x_vel, y_vel), object_collection_);
  object_collection_->add_object(ball);

  opponent_racket_ = std::make_shared<OpponentRacket>(TruePosition(x_size_ / 2, 1),
                                                      TruePosition(x_size_ / 2 + 10, 1),
                                                      object_collection_, ball);
  object_collection_->add_object(opponent_racket_);

  player_racket_ = std::make_shared<Racket>(TruePosition(x_size_ / 2, y_size_ - 2),
                                            TruePosition(x_size_ / 2 + 10, y_size_ - 2),
                                            object_collection_);
  object_collection_->add_object(player_racket_);

}

void PongGame::OnPlayerWallHit()
{
  //std::cout << "Player wall hit" << std::endl;
  game_over_ = true;
  you_won_ = false;
}

void PongGame::OnOpponentWallHit()
{
  //std::cout << "Opponent wall hit" << std::endl;
  game_over_ = true;
  you_won_ = true;
}

void PongGame::OnPlayerRacketHit()
{
  //std::cout << "Player racket hit" << std::endl;
}

void PongGame::OnOpponentRacketHit()
{
  //std::cout << "Opponent racket hit" << std::endl;
}

void PongGame::DefaultObjectHit()
{
  //static int count = 0;
  //count += 1;
  //std::cout << "Default object hit: " << count << std::endl;
}

std::tuple<double, double> PongGame::get_initial_ball_velocity()
{
  double x_value = tools::get_random_floating_point(0.0, 1.4531);
  double y_value = tools::get_random_floating_point(1.0435350, 1.4531);
  return std::make_tuple(tools::get_random_bool() ? x_value : -x_value,
                         tools::get_random_bool() ? y_value : -y_value);
}

bool PongGame::get_game_over_state() const
{
  return game_over_;
}

Winner PongGame::get_winner() const
{
  if (!game_over_)
  {
    return Winner::None;
  }
  else
  {
    return you_won_ ? Winner::You : Winner::Opponenet;
  }

}
