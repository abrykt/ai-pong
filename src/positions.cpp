#include "positions.hpp"

GridPosition true_to_grid(const TruePosition& true_position)
{
  return GridPosition((int)std::trunc(true_position[0]),
                      (int)std::trunc(true_position[1]));
}

GridPositions trues_to_grids(const TruePositions& true_postions)
{
  GridPositions grid_positions;
  for (auto&& item : true_postions.get_positions())
  {
    grid_positions.add_position(true_to_grid(item));
  }
  return grid_positions;
}


