#pragma once

#include <vector>
#include <iostream>
#include <cassert>
#include <utility>
#include <limits>
#include <stdexcept>

#include "tools/algorithm_helpers.hpp"
#include "tools/geometry.hpp"

template<typename T = double>
class Vector : public std::vector<T>
{
 public:

  explicit Vector() :
          std::vector<T>(2)
  {

  }

  explicit Vector(T x, T y) :
          std::vector<T>{x, y}
  {

  }

  explicit Vector(const std::vector<T>& arr) :
          Vector(arr[0], arr[1])
  {

  }

  Vector<T> operator+(const Vector<T>& other)
  {
    assert(other.size() == 2);
    return Vector<T>{this->at(0) + other.at(0), this->at(1) + other.at(1)};
  }

  Vector<T> operator*(const Vector<T>& other)
  {
    assert(other.size() == 2);
    return Vector<T>{this->at(0) * other.at(0), this->at(1) * other.at(1)};
  }

  Vector<T> operator/(const Vector<T>& other)
  {
    assert(other.size() == 2);
    auto divide = [](const T& num, const T& denum) -> T
    {
      T epsilon = std::numeric_limits<T>::epsilon();
      if (num < epsilon && denum < epsilon)
      {
        return 1;
      }
      else if (denum < epsilon)
      {
        throw std::overflow_error("Division by zero.");
      }
      else
      {
        return num / denum;
      }
    };
    return Vector<T>{divide(this->at(0), other.at(0)), divide(this->at(1), other.at(1))};
  }

  Vector<T> operator*(double multiplier)
  {
    return Vector<T>{this->at(0) * multiplier, this->at(1) * multiplier};
  }

  Vector<T> abs() const
  {
    return Vector<T>{std::abs(this->at(0)), std::abs(this->at(1))};
  }

  std::string to_string() const
  {
    return std::string("(" + std::to_string(this->at(0)) + "," + std::to_string(this->at(1)) + ")");
  }

  template<typename K>
  friend Vector<K> operator-(const Vector<K>& first, const Vector<K>& second);

  template<typename K>
  friend Vector<K> operator+(const Vector<K>& first, const Vector<K>& second);

  void operator+=(const Vector<T>& other)
  {
    assert(other.size() == 2);
    this->at(0) += other.at(0);
    this->at(1) += other.at(1);
  }

  Vector<T>& shift()
  {
    std::swap(this->at(1), this->at(0));
    return *this;
  }

  bool operator==(const Vector<T>& other)
  {
    using tools::are_same;
    assert(other.size() == 2);
    return are_same(other.at(0), this->at(0)) && are_same(other.at(1), this->at(1));
  }

  std::tuple<T, T> to_tuple() const
  {
    return std::tuple<T, T>(this->at(0), this->at(1));
  }

  Vector<T> to_normalized() const
  {
    return Vector<T>(tools::tuple_to_vector(tools::normalize(to_tuple())));
  }

  double norm() const
  {
    return tools::norm(to_tuple());
  }

  template<typename K>
  friend std::ostream& operator<<(std::ostream& os, const Vector<K>& position);
};

template<typename T>
Vector<T> operator-(const Vector<T>& first, const Vector<T>& second)
{
  assert(first.size() == 2 && second.size() == 2);
  return Vector<T>{first.at(0) - second.at(0), first.at(1) - second.at(1)};
}

template<typename T>
Vector<T> operator+(const Vector<T>& first, const Vector<T>& second)
{
  assert(first.size() == 2 && second.size() == 2);
  return Vector<T>{first.at(0) + second.at(0), first.at(1) + second.at(1)};
}

typedef Vector<int> GridPosition;
typedef Vector<double> TruePosition;
typedef Vector<double> Velocity;
typedef Vector<double> Angle; // TODO Maybe rename this to something else since it is not representing an angle.

inline Angle angle_to_vector(double radians)
{
  auto x = std::cos(radians);
  auto y = std::sin(radians);
  return Angle(x, y);
}

