#pragma once

#include <memory>
#include <vector>

#include "object_i.hpp"

class GameBoard;

typedef std::vector<std::shared_ptr<ObjectI>> HitObjects;

class ObjectCollectionI
{
 public:
  virtual ~ObjectCollectionI()  { }

  virtual void add_object(std::shared_ptr<ObjectI> object) = 0;
  virtual const std::vector<std::shared_ptr<ObjectI>>& get_objects() = 0;
  virtual void draw(std::shared_ptr<GameBoardI> board) = 0;
  virtual bool get_hit_objects(const ObjectI& other, HitObjects& hit_object) const = 0;
  virtual bool update_position_and_velocity(const ObjectI& other) const = 0;
  virtual bool find_first_object_that_was_hit(const ObjectI& other, std::shared_ptr<ObjectI>& hit_object) = 0;
};