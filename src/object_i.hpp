#pragma once

#include <memory>

#include "position.hpp"
#include "positions.hpp"

enum class ObjectType
{
  DEFAULT = 0,
  PLAYER_WALL,
  OPPONENT_WALL,
  PLAYER_RACKET,
  OPPONENT_RACKET,
  BALL

};

class GameBoardI;

class ObjectI
{
 public:
  virtual ~ObjectI()
  {

  }

  virtual void set_velocity(const Velocity& velocity) = 0;
  virtual Velocity get_velocity() const = 0;
  virtual const GridPositions& get_grid_positions() const = 0;
  virtual const TruePositions& get_true_positions() const = 0;
  virtual void clear_positions() = 0;
  virtual bool is_hit(const ObjectI& object) const = 0;
  virtual bool is_hit(const ObjectI& other, TruePosition& hit_position) const = 0;

  virtual Angle get_hit_angle(const ObjectI& object) const = 0;
  virtual void update() = 0;
  virtual void draw(std::shared_ptr<GameBoardI> game_board) = 0;
  virtual void move_object(const TruePosition& position) = 0;
  virtual std::vector<std::pair<TruePosition, TruePosition>> get_edges() const = 0;
  virtual Velocity get_velocity_after_bounce(const ObjectI& object) const = 0;
  virtual ObjectType get_object_type() const = 0;
  virtual void update_velocity_and_position_after_hit_with_object(std::shared_ptr<ObjectI> object) = 0;


};