#pragma once

#include <vector>

#include "tools/stl_helpers.hpp"

#include "game_board_i.hpp"
#include "positions.hpp"


class GameBoard : public GameBoardI
{
 public:
  explicit GameBoard(unsigned x_size, unsigned y_size);
  GameBoard() = delete;
  ~GameBoard() = default;
  DISABLE_COPY_ASSIGN_MOVE(GameBoard);
  void set(const GridPositions& positions);
  void clear_board();
  const std::vector<std::vector<bool>>& get_board()
  {
    return board_;
  }

 private:
  unsigned x_size_;
  unsigned y_size_;
  std::vector<std::vector<bool>> board_;

};