#pragma once

#include <memory>
#include "Eigen/Dense"

#include "tools/stl_helpers.hpp"

#include "game_board_i.hpp"
#include "positions.hpp"

class GameBoardEigen : public GameBoardI
{
 public:
  GameBoardEigen(size_t num_rows, size_t num_cols) :
          num_rows_(num_rows),
          num_cols_(num_cols),
          vector_size_(num_rows * num_cols)
  {
    clear_board();
  }

  DISABLE_COPY_ASSIGN_MOVE(GameBoardEigen);

  void set(const Positions<int>& positions) override
  {
    auto p = positions.get_positions();
    for (auto&& item : p)
    {
      int row = item[1];
      int col = item[0];
      (*pixels_)(num_cols_ * row + col) = 1.0;
    }
  }

  void clear_board() override
  {
    pixels_ = std::make_shared<Eigen::VectorXd>(vector_size_);
    pixels_->setZero();
  }

  std::shared_ptr<Eigen::VectorXd> get_vector()
  {
    return pixels_;
  }

 private:
  size_t num_rows_;
  size_t num_cols_;
  size_t vector_size_;
  std::shared_ptr<Eigen::VectorXd> pixels_;
};

