#pragma once

#include <random>

#include "Eigen/Dense"
#include <tools/ml/forward_i.hpp>
#include "tools/stl_helpers.hpp"
#include "tools/ml/gradient_descent_optimizers.hpp"

#include "pong.hpp"
#include "pong_game_factory.hpp"
#include "gameboard_eigen.hpp"

int direction_to_int(Direction direction)
{
  // 2 is up in ai gym, set to 1. This is left for us.
  // 3 is down, set to 0.
  // They choose 2 if sample is lower than probability. Same as I.
  return direction == Direction::Left ? 1 : 0;
}


class PongPlayer
{
 public:
  typedef std::vector<std::shared_ptr<Eigen::VectorXd>> TrainingDataType;
  typedef std::vector<double> StoredProbabilitiesType;
  typedef std::vector<int> StoredDecisionType;

  PongPlayer(unsigned num_rows, unsigned num_cols, tools::ForwardI& forward_alg) :
          gen_(rd_()),
          uniform_distribution(0, 1),
          forward_alg_(forward_alg)
  {
    game_board_ = std::make_shared<GameBoardEigen>(num_rows, num_cols);
    pong_game_ = PongGameFactory::create_pong_game(num_cols, num_rows);
  }

  DISABLE_COPY_ASSIGN_MOVE(PongPlayer);

  bool play_game()
  {
    clear_data();
    pong_game_->clear_game();

    std::shared_ptr<Eigen::VectorXd> last_frame = nullptr;
    bool is_game_over = false;
    Winner winner = Winner::None;
    while (!is_game_over)
    {
      game_board_->clear_board();
      pong_game_->draw_current_board(game_board_);
      std::shared_ptr<Eigen::VectorXd> this_frame = game_board_->get_vector();
      game_data_.push_back(this_frame);

      std::shared_ptr<Eigen::VectorXd> input;
      if (last_frame != nullptr)
      {
        input.reset(new Eigen::VectorXd(*this_frame - *last_frame));
      }
      else
      {
        input.reset(new Eigen::VectorXd(this_frame->rows(), this_frame->cols()));
        input->setZero();
      }
      last_frame = this_frame;
      double left_probability = get_left_probability(input);
      LOG_DEBUG("left_probability: %f", left_probability);
      Direction direction = stochastic_action(left_probability);

      probabilities_.push_back(left_probability);
      decisions_.push_back(direction_to_int(direction));
      training_data_.push_back(input);

      pong_game_->move_racket(direction);
      pong_game_->update();
      winner = pong_game_->get_winner();
      switch (winner)
      {
        case Winner::You:
        {
          outcome_.push_back(1);
          break;
        }
        case Winner::Opponenet:
        {
          outcome_.push_back(-1);
          break;
        }
        case Winner::None:
        {
          outcome_.push_back(0);
          break;
        }
      }
      is_game_over = pong_game_->get_game_over_state();
    }
    TOOLS_ASSERT(winner != Winner::None, "Someone should have won.");
    return winner == Winner::You;
  }

  const TrainingDataType& get_training_data()
  {
    return training_data_;
  }

  const TrainingDataType& get_game_data()
  {
    return game_data_;
  }

  const StoredProbabilitiesType& get_probabilites()
  {
    return probabilities_;
  };

  const StoredDecisionType& get_decisions()
  {
    return decisions_;
  }

  const StoredProbabilitiesType& get_outcomes()
  {
    return outcome_;
  }

 private:

  double get_left_probability(std::shared_ptr<Eigen::VectorXd>& game_board)
  {
    return forward_alg_.forward(*game_board)(0);
  }

  void clear_data()
  {
    training_data_.clear();
    decisions_.clear();
    probabilities_.clear();
    outcome_.clear();
    game_data_.clear();
  }

  Direction stochastic_action(double left_probability)
  {
    return uniform_distribution(gen_) < left_probability ? Direction::Left : Direction::Rigth;
  }

  PongGamePtr pong_game_;
  std::shared_ptr<GameBoardEigen> game_board_;
  TrainingDataType training_data_;
  TrainingDataType game_data_;
  StoredProbabilitiesType probabilities_;
  StoredDecisionType decisions_;
  StoredProbabilitiesType outcome_;
  std::random_device rd_;
  std::mt19937 gen_;
  std::uniform_real_distribution<double> uniform_distribution;
  tools::ForwardI& forward_alg_;
};


