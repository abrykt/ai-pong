#pragma once

#include "line.hpp"
#include "ball.hpp"

enum class RacketDirection
{
  None,
  left,
  right,
};

class Racket : public Line
{
 public:
  explicit Racket(const TruePosition& start, const TruePosition& end) :
          Line(start, end),
          direction_(RacketDirection::None)
  {
  }

  explicit Racket(const TruePosition& start, const TruePosition& end, std::shared_ptr<ObjectCollectionI> object_collection) :
          Line(start, end, Velocity(0, 0), object_collection),
          direction_(RacketDirection::None)
  {
  }

  Racket() = delete;

  DISABLE_COPY_ASSIGN_MOVE(Racket);

  virtual ~Racket() = default;

  bool is_side_hit(std::shared_ptr<ObjectI>& hit_object)
  {
    auto collection = get_object_collection().lock();
    if (collection->find_first_object_that_was_hit(*this, hit_object))
    {
      return hit_object->get_object_type() == ObjectType::DEFAULT;
    }
    else
    {
      return false;
    }
  }

  void set_velocity(const Velocity& velocity) override
  {
    if (velocity.norm() < 2) // TODO Might remove this since velocity of racket is fixed.
    {
      Object::set_velocity(velocity);
    }
  }

  virtual void update() override
  {
    std::shared_ptr<ObjectI> hit_object;
    if (is_side_hit(hit_object) && direction_ != RacketDirection::None)
    {
      Velocity new_velocity;
      hit_object->is_hit(*this, new_velocity);
      if (new_velocity.norm() > 1)
      {
        move_object(new_velocity * ALMOST_ALL_THE_WAY);
      }
      set_velocity(Velocity(0, 0));
      direction_ = RacketDirection::None; // TODO Look into if this is used at all. Might always be reset later.
    }
    else
    {
      Line::update();
    }
  }

  void increase_speed_left()
  {
    set_velocity(Velocity(-get_racket_speed(), 0));
    direction_ = RacketDirection::left;
  }

  void increase_speed_right()
  {
    set_velocity(Velocity(get_racket_speed(), 0));
    direction_ = RacketDirection::right;
  }


  ObjectType get_object_type() const override
  {
    return ObjectType::PLAYER_RACKET;
  }

 private:

  virtual double get_racket_speed() const
  {
    return 1.9;
  }

  RacketDirection direction_;

};