#pragma once

#include "racket.hpp"

class OpponentRacket : public Racket
{
 public:
  explicit OpponentRacket(const TruePosition& start, const TruePosition& end,
                 std::shared_ptr<ObjectCollectionI> object_collection,
                 std::shared_ptr<Ball> ball) :
          Racket(start, end, object_collection),
          ball_(ball),
          length_(end.at(0) - start.at(0))
  {

  }

  OpponentRacket() = delete;
  ~OpponentRacket() = default;

  DISABLE_COPY_ASSIGN_MOVE(OpponentRacket);

  void update() override
  {
    RacketDirection direction = get_direction_from_looking_at_ball();
    if (direction == RacketDirection::left)
    {
      increase_speed_left();
    }
    else if (direction == RacketDirection::right)
    {
      increase_speed_right();
    }
    Racket::update();
  }

  ObjectType get_object_type() const override
  {
    return ObjectType::OPPONENT_RACKET;
  }

 private:

  RacketDirection get_direction_from_looking_at_ball()
  {
    auto positions = ball_->get_grid_positions();
    auto pos = positions.get_positions()[0]; // Should only be one here;
    auto ball_x_pos = pos.at(0);
    int my_x_pos_start = get_grid_positions().get_positions()[0].at(0);
    int middle_x = my_x_pos_start + length_ / 2;
    if (middle_x < ball_x_pos)
    {
      return RacketDirection::right;
    }
    else if (middle_x > ball_x_pos)
    {
      return RacketDirection::left;
    }
    else
    {
      return RacketDirection::None;
    }
  }

  std::shared_ptr<Ball> ball_;
  unsigned length_;
};