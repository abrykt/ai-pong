#pragma once

#include <vector>
#include <memory>
#include <mutex>
#include <atomic>

#include "Eigen/Dense"
#include "tools/stl_helpers.hpp"
#include "tools/ml/activation_functions.hpp"
#include <tools/ml/discount_reward.hpp>
#include "tools/ml/gradient_descent_optimizers.hpp"
#include "tools/log.hpp"

#include "game_vizualizer.hpp"
#include "pong_player.hpp"
#include "pong.hpp"


using tools::RmsPropPolicyGradientOptimizer;
using tools::ActivationFunctionType;
using tools::ObservationWithDecision;

enum class ShowProgressInUi
{
  yes,
  no
};

class PongLearner
{
 public:

  PongLearner() :
          num_rows(40),
          num_cols(40)
  {
    unsigned num_pixels = num_rows * num_cols;
    std::vector<unsigned> layer_structure{num_pixels, 200, 1};

    discount_factor_ = 0.99;          // this set how much reward we should have instantly


    std::vector<ActivationFunctionType> activation_functions{ActivationFunctionType::Relu,
                                                             ActivationFunctionType::Sigmoid};
    std::vector<ActivationFunctionType> activation_function_prims{ActivationFunctionType::Relu,
                                                                  ActivationFunctionType::Unity};
    pg_rms_prop_ = std::unique_ptr<RmsPropPolicyGradientOptimizer>(new RmsPropPolicyGradientOptimizer(layer_structure,
                                                          activation_functions,
                                                          activation_function_prims));
    pong_player_ = std::unique_ptr<PongPlayer>(new PongPlayer(num_rows, num_cols, *pg_rms_prop_));
  }

  DISABLE_COPY_ASSIGN_MOVE(PongLearner);

  void learn_pong_and_show_games(ShowProgressInUi show_progress)
  {
    should_quit_.store(false);
    std::thread game_learner_thread(&PongLearner::learn_pong, this);
    // I got some weird deadlocks when I was running gui in the thread that is created here... so I switched to
    // running training in the thread instead. See https://stackoverflow.com/a/22058947/2996272
    if(show_progress == ShowProgressInUi::yes)
    {
      show_last_game_played();
      // Above call will not end.
      // Below is only used when recompiling with a fix number of games to show.
      should_quit_.exchange(true);
    }
    game_learner_thread.join();
  }

 private:

  void learn_pong()
  {
    LOG_SCOPED();
    unsigned num_wins = 0;
    unsigned num_loss = 0;
    unsigned num_rounds = 0;
    unsigned total_num_wins = 0;
    unsigned total_num_loss = 0;

    while (!should_quit_.load())
    {
      if (pong_player_->play_game())
      {
        ++num_wins;
        ++total_num_wins;
      }
      else
      {
        ++num_loss;
        ++total_num_loss;
      }

      {
        std::lock_guard<std::mutex> lg(last_game_played_mutex_);
        last_game_played_ = pong_player_->get_game_data();
      }

      outcomes_.insert(outcomes_.begin(),
                       pong_player_->get_outcomes().begin(),
                       pong_player_->get_outcomes().end());
      training_data_.insert(training_data_.begin(),
                            pong_player_->get_training_data().begin(),
                            pong_player_->get_training_data().end());
      decisions_.insert(decisions_.begin(),
                        pong_player_->get_decisions().begin(),
                        pong_player_->get_decisions().end());
      probabilities_.insert(probabilities_.begin(),
                            pong_player_->get_probabilites().begin(),
                            pong_player_->get_probabilites().end());

      if (num_wins == games_per_round_ || num_loss == games_per_round_)
      {
        std::lock_guard<std::mutex> lg(last_game_played_mutex_); // Keeps ui from showing already shown games.
        ++num_rounds;
        LOG_INFO("rounds played %d, won since last update %d, lost %d", num_rounds, num_wins, num_loss);

        num_wins = 0;
        num_loss = 0;

        auto discounted_reward = tools::discount_reward(outcomes_, discount_factor_, tools::ShouldNormalize::Yes);
        const auto observations = make_observations(training_data_,
                                                    discounted_reward,
                                                    decisions_,
                                                    probabilities_);
        for (auto&& observation : observations)
        {
          pg_rms_prop_->add_training_data(observation);
        }

        pg_rms_prop_->train();

        outcomes_.clear();
        training_data_.clear();
        decisions_.clear();
        probabilities_.clear();

        if (num_rounds % batch_size_ == 0)
        {
          pg_rms_prop_->apply_gradients();
        }
      }
    }
  }

  void show_last_game_played()
  {
    GameVisualizer gv(num_rows, num_cols);
    PongPlayer::TrainingDataType game_data;
    //for (unsigned i = 0; i < 3; ++i)
    while (true)
    {
      {
        std::lock_guard<std::mutex> lg(last_game_played_mutex_);
        game_data = std::move(last_game_played_);
      }
      if (!game_data.empty())
      {
        gv.show_game(game_data);
      }
    }
  }

  typedef std::vector<ObservationWithDecision::ObservationPtr> ObservationsType;

  static ObservationsType make_observations(const std::vector<std::shared_ptr<tools::Vector>>& pixels,
                                            const std::vector<double>& discounted_awards,
                                            const std::vector<int>& decisions,
                                            const std::vector<double>& probabilities)
  {
    size_t num_observations = pixels.size();
    TOOLS_ASSERT(num_observations == discounted_awards.size(), "Incorret size for discounted awards.");
    TOOLS_ASSERT(num_observations == decisions.size(), "Incorret size for decisions.");
    TOOLS_ASSERT(num_observations == probabilities.size(), "Incorret size for probabilities.");

    ObservationsType observations(num_observations);
    for (size_t i = 0; i < num_observations; ++i)
    {
      tools::Vector error(1);
      error(0) = (decisions[i] - probabilities[i]) * discounted_awards[i];
      observations[i] = std::make_shared<ObservationWithDecision::ObservationType>(std::make_tuple(pixels[i], error));
    }
    return observations;
  }

  const unsigned batch_size_ = 10;
  const unsigned games_per_round_ = 21;
  unsigned num_rows;
  unsigned num_cols;
  std::unique_ptr<RmsPropPolicyGradientOptimizer> pg_rms_prop_;
  std::unique_ptr<PongPlayer> pong_player_;
  double discount_factor_;                       // this set how much reward we should have instantly
  std::mutex last_game_played_mutex_;
  PongPlayer::TrainingDataType last_game_played_;
  std::atomic<bool> should_quit_;

  PongPlayer::StoredProbabilitiesType outcomes_;
  PongPlayer::TrainingDataType training_data_;
  PongPlayer::StoredDecisionType decisions_;
  PongPlayer::StoredProbabilitiesType probabilities_;
};