#pragma once

#include <cassert>
#include <cmath>
#include <functional>


#include "object.hpp"
#include "object_collection_i.hpp"
#include "line.hpp"
#include "position.hpp"

#define ALMOST_ALL_THE_WAY 0.99999999

class Ball : public Line
{
 public:
  explicit Ball(const TruePosition& position,
                const Velocity& velocity) :
          Line(position, position, velocity)
  {
    reset_set_velocity();
    reset_get_velocity();
  }


  explicit Ball(const TruePosition& position,
                const Velocity& velocity,
                const std::shared_ptr<ObjectCollectionI>& object_collection) :
          Line(position, position, velocity, object_collection)
  {
    reset_set_velocity();
    reset_get_velocity();
  }

  virtual ~Ball() = default;

  Ball() = delete;
  DISABLE_COPY_ASSIGN_MOVE(Ball);


  Velocity get_velocity() const override
  {
    return velocity_getter_();
  }

  void set_velocity(const Velocity& velocity) override
  {
    velocity_setter_(velocity);
  }

  void update()
  {
    auto check_for_hit_objects = [&](std::shared_ptr<ObjectI>& hit_objects)
    {
      auto locked_objects = get_object_collection().lock();
      bool hit_something = locked_objects->find_first_object_that_was_hit(*this, hit_objects);
      return hit_something;
    };
    std::shared_ptr<ObjectI> hit_object;
    Velocity old_velocity = get_velocity();
    bool hit_something = false;
    Velocity temp_velocity;

    velocity_setter_ = [&](const Velocity& velocity)
    {
      temp_velocity = velocity;
    };

    velocity_getter_ = [&]()
    {
      return temp_velocity;
    };

    velocity_setter_(old_velocity);

    while (check_for_hit_objects(hit_object))
    {
      hit_something = true;
      // could maybe create a copy of "this" object and use that to compute the velocity / position after hit
      // instead of redirecting get and set velocity.
      update_velocity_and_position_after_hit_with_object(hit_object);
    }

    Line::update();

    Velocity without_old = get_velocity();
    reset_set_velocity();
    reset_get_velocity();
    set_velocity(without_old);

    if (hit_something)
    {
      Velocity restored;
      Velocity diff = old_velocity.abs() - temp_velocity.abs();
      // Put the old components back.
      restored.at(0) = add_magnitude_with_respect_to_sign(without_old.at(0), diff.at(0));
      restored.at(1) = add_magnitude_with_respect_to_sign(without_old.at(1), diff.at(1));
      set_velocity(restored);
    }
  }

  ObjectType get_object_type() const override
  {
    return ObjectType::BALL;
  }

  void update_velocity_and_position_after_hit_with_object(std::shared_ptr<ObjectI> object)
  {
    TruePosition movement_to_hit;
    bool hit = object->is_hit(*this, movement_to_hit);
    assert(hit); TAG_UNUSED(hit);
    move_object(movement_to_hit * ALMOST_ALL_THE_WAY);
    Velocity current_velocity = get_velocity();
    distance_left_ = current_velocity - movement_to_hit; // Should include time also. Now time is always 1.
    set_velocity(distance_left_);
    distance_left_ = object->get_velocity_after_bounce(*this);
    set_velocity(distance_left_);
  }

 private:
  double add_magnitude_with_respect_to_sign(double first, double second)
  {
    double result = 0.0;
    double abs_value = std::abs(second);
    bool first_is_positive = !std::signbit(first);
    bool second_is_positive = !std::signbit(second);
    if (first_is_positive && second_is_positive)
    {
      result = first + abs_value;
    }
    else if (first_is_positive && !second_is_positive)
    {
      result = first + abs_value;
    }
    else if (!first_is_positive && second_is_positive)
    {
      result = first - abs_value;
    }
    else if (!first_is_positive && !second_is_positive)
    {
      result = first - abs_value;
    }
    return result;
  }

  void reset_set_velocity()
  {
    velocity_setter_ = [this](const Velocity& velocity)
    {
      Object::set_velocity(velocity);
    };
  }

  void reset_get_velocity()
  {
    velocity_getter_ = [this]()
    {
      return Object::get_velocity();
    };
  }

  std::function<Velocity()> velocity_getter_;
  std::function<void(const Velocity&)> velocity_setter_;
  Velocity distance_left_;

};