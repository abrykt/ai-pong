include(CMakeParseArguments)

# Adds a set of files to  a variable in the parent scope.
# The name of the variabel containing the files to add cannot be the same as the variable inthe parent scope.
# Example:
#   set(FILES
#   pong_ut.cpp)
#   add_files_to_parent_scope(PARENT_VARIABLE UT_FILES FILES ${UT_FILES})
macro(add_files_to_parent_scope)
    cmake_parse_arguments(
            PARSED_ARG
            ""
            "PARENT_VARIABLE"
            "FILES;"
            ${ARGN}
    )
    foreach(FILE ${PARSED_ARG_FILES})
        list(APPEND ${PARSED_ARG_PARENT_VARIABLE} ${CMAKE_CURRENT_SOURCE_DIR}/${FILE})
    endforeach()
    set(${PARSED_ARG_PARENT_VARIABLE} ${${PARSED_ARG_PARENT_VARIABLE}} PARENT_SCOPE)
endmacro()

function(JOIN OUTPUT GLUE)
    set(_TMP_RESULT "")
    set(_GLUE "") # effective glue is empty at the beginning
    foreach(arg ${ARGN})
        set(_TMP_RESULT "${_TMP_RESULT}${_GLUE}${arg}")
        set(_GLUE "${GLUE}")
    endforeach()
    set(${OUTPUT} "${_TMP_RESULT}" PARENT_SCOPE)
endfunction()

#function(JOIN VALUES GLUE OUTPUT)
#    message(${VALUES})
#    set(TEMP "")
#    foreach(V ${VALUES})
#        message(${V})
#    endforeach()
#    #string (REGEX REPLACE "([^\\]|^);" "\\1${GLUE}" _TMP_STR "${VALUES}")
#    #string (REGEX REPLACE "[\\](.)" "\\1" _TMP_STR "${_TMP_STR}") #fixes escaping
#    #set (${OUTPUT} "${_TMP_STR}" PARENT_SCOPE)
#endfunction()