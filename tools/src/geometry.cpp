//
// Created by brykt on 2017-11-20.
//

#include "tools/geometry.hpp"

namespace tools
{

Point<double> rotate(const Point<double>& point, double radians)
{
  /* Rotation matrix R = [cos(radians), -sin(radians);
   *                      sin(radians), cos(radians)]  */
  double x = std::get<0>(point);
  double y = std::get<1>(point);
  double new_x = std::cos(radians) * x - std::sin(radians) * y;
  double new_y = std::sin(radians) * x + std::cos(radians) * y;
  return std::make_tuple(new_x, new_y);
}

}
