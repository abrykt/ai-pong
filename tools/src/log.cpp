#include <stdio.h>
#include "tools/log.hpp"

#ifdef TOOLS_LOGFILE_NAME
std::string log_filename = TOOLS_LOGFILE_NAME
#else
std::string log_filename = "application.log";
#endif

LogStream get_log()
{
  static InternalThreadSafeLogStream log(log_filename.c_str());
  return log;
}

InternalThreadSafeLogStream::InternalThreadSafeLogStream(const char* filename) :
        stream_(filename)
{
  thread_safe_write("Logger created.\n");
}

InternalThreadSafeLogStream::~InternalThreadSafeLogStream()
{
  thread_safe_write("Logger deleted.\n");
}

void InternalThreadSafeLogStream::thread_safe_write(std::string s)
{
  std::lock_guard<std::mutex> lock_guard(lock_);
  stream_ << s;
  // TODO Keep singleton logger in smarter storage so that log lines are guaranteed to be flushed on ctrl- c
  stream_.flush();
}

std::string GetFileName(const char* path)
{
#ifdef WIN32
  char filename[30];
  char ext[4];
  _splitpath(path, NULL, NULL, filename, ext);
  return std::string(filename) + std::string(ext);
#endif
#ifdef __linux__
  char* filename = basename(const_cast<char*>(path));
  return std::string(filename);
#endif
}

std::string GetLogTimeStamp()
{
  using namespace std::chrono;
  high_resolution_clock::time_point p = high_resolution_clock::now();
  milliseconds ms = duration_cast<milliseconds>(p.time_since_epoch());
  seconds s = duration_cast<seconds>(ms);
  std::time_t t = s.count();
  std::size_t fractional_seconds = ms.count() % 1000;
  std::string time_without_ms = std::string(std::ctime(&t));
  time_without_ms.resize(time_without_ms.size() - 6);
  std::stringstream ss;
  ss << time_without_ms << ":" << fractional_seconds;
  return ss.str();
}