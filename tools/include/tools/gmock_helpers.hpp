#pragma once

#include <tuple>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

// TODO Add test for the below functions.

MATCHER_P(FloatNearPointwise, tol, "Out of range")
{
  return (std::get<0>(arg) > std::get<1>(arg) - tol && std::get<0>(arg) < std::get<1>(arg) + tol);
}

#define EXPECT_ARRAY_DOUBLE_EQUAL(known, actual, tolerance)                                   \
do {                                                                                          \
  EXPECT_THAT(known, ::testing::Pointwise(FloatNearPointwise(tolerance), actual));            \
}while(false)


#define EXPECT_ARRAY_ARRAY_DOUBLE_EQUAL(known, actual, tolerance)                             \
do {                                                                                          \
  ASSERT_EQ(known.size(), actual.size());                                                     \
  auto actual_iter = actual.begin();                                                          \
  for (auto& item : known)                                                                    \
  {                                                                                           \
    EXPECT_ARRAY_DOUBLE_EQUAL(*actual_iter, item, tolerance);                                 \
    std::advance(actual_iter, 1);                                                             \
  }                                                                                           \
}while(false)
