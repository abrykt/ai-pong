#pragma once

#include <tuple>
#include <vector>
#include <tuple>

namespace tools
{


typedef std::tuple<double, double> PixelPoint;
typedef std::tuple<unsigned, unsigned, unsigned> Color;
typedef std::vector <std::vector<Color>> PixelMatrix;

#define Black Color(0, 0, 0)
#define White Color(255, 255, 255)
#define Red Color(255, 0, 0)

class GuiI
{
 public:
  virtual bool InitEverything() = 0;
  virtual void Clear() = 0;
  virtual void Render() = 0;
  virtual void draw_line(const PixelPoint& first, const PixelPoint& second) = 0;
  virtual void draw_point(const PixelPoint& point, Color color, float point_size) = 0;
  virtual void draw_matrix(const PixelMatrix& matrix, PixelPoint start_point, float point_size) = 0;
};

}