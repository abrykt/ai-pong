#pragma once

#include <algorithm>
#include <cmath>
#include <numeric>
#include <tuple>
#include <vector>

namespace tools
{

template<typename T>
bool are_same(const T& lhs, const T& rhs)
{
  return (std::abs(lhs - rhs) <= std::numeric_limits<T>::epsilon());
}

template<typename T>
bool contains_zero(const std::vector<T>& v)
{
  auto is_zero = [](const T& value)
  {
    return are_same(value, static_cast<T>(0));
  };
  return std::find_if(v.begin(), v.end(), is_zero) != v.end();
}

template<typename T>
bool contains_zero(const std::vector<std::vector<T>>& v)
{
  for (auto&& item : v)
  {
    if (contains_zero(item))
    {
      return true;
    }
  }
  return false;
}


template<typename InputIterator, typename ValueType>
InputIterator closest(InputIterator first, InputIterator last, ValueType value)
{
  auto minimum = std::min_element(first, last, [value](ValueType x, ValueType y)
  {
    return (std::abs(x - value) < std::abs(y - value));
  });
  return minimum;
}


template<typename T>
struct incrementer
{
  incrementer(T start_value, T step_size) :
          value_(start_value),
          step_size_(step_size)
  {

  }

  operator T() const
  {
    return value_;
  }

  incrementer& operator++()
  {
    value_ += step_size_;
    return *this;
  }

 private:
  T value_;
  T step_size_;
};

template<typename InputIterator, typename ValueType>
void iota_T(InputIterator start_iter, InputIterator end_iter, ValueType value_start, ValueType step)
{
  std::iota(start_iter, end_iter, incrementer<ValueType>(value_start, step));
}

}