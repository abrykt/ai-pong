#pragma once

#include <tuple>
#include <iostream>
#include <vector>
#include <stdexcept>

#ifndef WIN32

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include "gui_i.hpp"
#include "log.hpp"

#endif

namespace tools
{

#define THROW_ON_ERROR(function) if (0 != function) {throw std::runtime_error(SDL_GetError());}


#if defined(_WIN32)
class Gui : public GuiI
{
public:

  bool InitEverything(unsigned sizeX, unsigned sizeY, Color background_color) { return true; }
  void Clear() { }
  void Render() { }
  void draw_point(const PixelPoint& point, Color color, unsigned point_size) { }
  void draw_line(const PixelPoint& first, const PixelPoint& second) {  }
  void draw_matrix(const PixelMatrix& matrix, PixelPoint start_point, unsigned point_size) { }
};

#else

// Inspired by http://headerphile.blogspot.se/2014/04/part-3-game-programming-in-sdl2-drawing.html
class Gui : public GuiI
{
 public:
  Gui() = delete;

  Gui(unsigned sizeX, unsigned sizeY, Color background_color) :
          sizeX_(sizeX),
          sizeY_(sizeY),
          background_color_(background_color)
  {
    if (!InitEverything())
    {
      throw std::exception();
    }
  }

  ~Gui()
  {
    SDL_GL_DeleteContext(context_);
    SDL_DestroyWindow(window_);
    SDL_Quit();
  }

  bool InitEverything()
  {
    if (!InitSDL() || !CreateWindow() || !InitAttributes()) //) || !CreateRenderer())
    {
      throw std::exception();
    }

    glViewport(0, 0, sizeX_, sizeY_);                         // Reset Viewport
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity(); // Reset The Projection Matrix
    glOrtho(0.0f, sizeX_, sizeY_, 0.0f, -1.0f, 1.0f);           // Create Ortho View (0,0 At Top Left)
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();  // Reset The Modelview Matrix

    glClearColor(0.0f, 0.0f, 0.2f, 1.0f);
    //SetupRenderer(background_color_);
    //Clear();
    return true;
  }

  void Clear()
  {
    //SDL_RenderClear(renderer);    // Clear the window and make it all green
    //SDL_RenderPresent(renderer);  // Render the changes above
  }

  void Render()
  {
    SDL_GL_SwapWindow(window_);
    //SDL_RenderPresent(renderer); // Render the changes above
  }

  void draw_point(const PixelPoint& point, Color color, float point_size)
  {
    double x, y;
    std::tie(x, y) = point;
    glPointSize(point_size);
    unsigned r, g, b;
    std::tie(r, g, b) = color;
    glColor3ub(r, g, b);
    glBegin(GL_POINTS);
    glVertex2f(x, y);
    glEnd();
  }

  void draw_line(const PixelPoint& first, const PixelPoint& second)
  {
    double x0, y0, x1, y1;
    std::tie(x0, y0) = first;
    std::tie(x1, y1) = second;

    glLineWidth(2);
    glColor3ub(255, 0, 0);
    glBegin(GL_LINES);
    glVertex2f(x0, y0);
    glVertex2f(x1, y1);
    glEnd();
  }

  void draw_matrix(const PixelMatrix& matrix, PixelPoint start_point, float point_size)
  {
    unsigned x_start, y_start, x, y;
    std::tie(x_start, y_start) = start_point;
    std::tie(x, y) = start_point;

    for (auto&& item : matrix)
    {
      x = x_start;
      for (auto&& item2 : item)
      {
        PixelPoint p(x, y);
        draw_point(p, item2, point_size);
        x += point_size;
      }
      y += point_size;
    }
  }

 private:

  bool InitSDL()
  {
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
    {
      LOG_ERROR("Failed to initialize SDL : %s", SDL_GetError());
      return false;
    }
    return true;
  }

  bool InitAttributes()
  {
    THROW_ON_ERROR(SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8));
    THROW_ON_ERROR(SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8));
    THROW_ON_ERROR(SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8));
    THROW_ON_ERROR(SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8));
    THROW_ON_ERROR(SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1));
    THROW_ON_ERROR(SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24));
    THROW_ON_ERROR(SDL_GL_SetSwapInterval(1)); //Enable Vsync
    return true;
  }

  bool CreateWindow()
  {
    window_ = SDL_CreateWindow("A window",
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              sizeX_,
                              sizeY_,
                              SDL_WINDOW_OPENGL);

    context_ = SDL_GL_CreateContext(window_);
    if (context_ == nullptr)
    {
      LOG_ERROR("Failed to create context : %s", SDL_GetError());
      return false;
    }

    if (window_ == nullptr)
    {
      LOG_ERROR("Failed to create window : %s ", SDL_GetError());
      return false;
    }
    return true;
  }

  bool CreateRenderer()
  {
    renderer_ = SDL_CreateRenderer(window_, -1, 0);
    if (renderer_ == nullptr)
    {
      LOG_ERROR("Failed to create renderer : %s", SDL_GetError());
      return false;
    }
    return true;
  }

  void SetupRenderer(Color background_color)
  {
    // Set size of renderer to the same as window
    THROW_ON_ERROR(SDL_RenderSetLogicalSize(renderer_, sizeX_, sizeY_));
    // Set color of renderer to green
    int r, g, b;
    std::tie(r, g, b) = background_color;
    THROW_ON_ERROR(SDL_SetRenderDrawColor(renderer_, r, g, b, 0));
  }

  int posX = 100;
  int posY = 200;
  unsigned sizeX_;
  unsigned sizeY_;
  Color background_color_;

  SDL_Window* window_;
  SDL_GLContext context_;
  SDL_Renderer* renderer_;
};

#endif

}