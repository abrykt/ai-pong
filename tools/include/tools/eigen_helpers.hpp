#pragma once

#include <vector>
#include "Eigen/Dense"

namespace tools
{
inline void std_vector_to_eigen_matrix(const std::vector<std::vector<double>>& vectors, Eigen::MatrixXd& matrix)
{
  size_t num_rows = vectors.size();
  size_t num_columns = vectors.at(0).size();
  matrix.resize(num_rows, num_columns);
  for (size_t row_index = 0; row_index < num_rows; ++row_index)
  {
    for (size_t col_index = 0; col_index < num_columns; ++col_index)
    {
      matrix(row_index, col_index) = vectors.at(row_index).at(col_index);
    }
  }
}

inline void eigen_matrix_to_std_vector(const Eigen::MatrixXd& matrix, std::vector<std::vector<double>>& vectors)
{
  long num_rows = matrix.rows();
  long num_columns = matrix.cols();
  vectors.resize(num_rows, std::vector<double>(num_columns));
  for (long row_index = 0; row_index < num_rows; ++row_index)
  {
    for (long col_index = 0; col_index < num_columns; ++col_index)
    {
      vectors.at(row_index).at(col_index) = matrix(row_index, col_index);
    }
  }
}

inline std::vector<Eigen::VectorXd> std_to_eigen(const std::vector<std::vector<double>>& std_vector)
{
  std::vector<Eigen::VectorXd> eigen_vector;
  eigen_vector.resize(std_vector.size());
  std::transform(std_vector.begin(), std_vector.end(), eigen_vector.begin(), [](const std::vector<double>& std_vec)
  {
    return Eigen::Map<const Eigen::VectorXd, Eigen::Unaligned>(std_vec.data(),
                                                               std_vec.size());
  });
  return eigen_vector;
}

inline std::vector<Eigen::MatrixXd> std_to_eigen(const std::vector<std::vector<std::vector<double>>>& std_vector)
{
  std::vector<Eigen::MatrixXd> eigen_vector;
  eigen_vector.resize(std_vector.size());
  std::transform(std_vector.begin(), std_vector.end(), eigen_vector.begin(),
                 [](const std::vector<std::vector<double>>& std_vec)
                 {
                   Eigen::MatrixXd matrix;
                   std_vector_to_eigen_matrix(std_vec, matrix);
                   return matrix;
                 });
  return eigen_vector;
}

inline std::vector<std::vector<double>> eigen_to_std(const std::vector<Eigen::VectorXd> eigen_vec,
                                              std::vector<std::vector<double>>& std_vec)
{
  std_vec.clear();
  std_vec.resize(eigen_vec.size());
  std::transform(eigen_vec.begin(), eigen_vec.end(), std_vec.begin(), [](const Eigen::VectorXd& vec)
  {
    return std::vector<double>(vec.data(), vec.data() + vec.size());
  });
  return std_vec;
}

inline std::vector<std::vector<std::vector<double>>> eigen_to_std(const std::vector<Eigen::MatrixXd>& eigen_vec,
                                                           std::vector<std::vector<std::vector<double>>>& std_vec)
{
  std_vec.clear();
  std_vec.resize(eigen_vec.size());
  std::transform(eigen_vec.begin(), eigen_vec.end(), std_vec.begin(), [](const Eigen::MatrixXd& mat)
  {
    std::vector<std::vector<double>> temp;
    eigen_matrix_to_std_vector(mat, temp);
    return temp;
  });
  return std_vec;
}

inline double standard_deviation(Eigen::VectorXd& values)
{
  typedef Eigen::VectorXd Vector;
  long N = values.size();
  Vector normed = values - Vector::Ones(N) * values.mean();
  return std::sqrt(normed.array().pow(2).sum() / float(N));
}

inline Eigen::VectorXd normalize(Eigen::VectorXd& values)
{
  return (values -  Eigen::VectorXd::Ones(values.size()) * values.mean()) / tools::standard_deviation(values);
}


}


