#pragma once

#include <cassert>
#include <random>

#include "log.hpp"

namespace tools
{


template<typename T>
T get_random_int(T min, T max)
{
  TOOLS_ASSERT(min < max, "Min should be smaller than max.");
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution <T> dis(min, max);
  return dis(gen);
}

template<typename T>
T get_random_floating_point(T min, T max)
{
  TOOLS_ASSERT(min < max, "Min should be smaller than max.");
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<T> dis(min, max);
  return dis(gen);
}

inline bool get_random_bool(double probability_of_true = 0.5)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dis(0, 1);
  return dis(gen) < probability_of_true;
}

}