#pragma once

#ifdef WIN32
#pragma warning(disable:4996)
#endif

#include "stdlib.h"
#include <stdarg.h>
#include <sstream>
#include <thread>
#include <iostream>
#include <fstream>
#include <boost/format.hpp>
#include <stdio.h>
#include <assert.h>
#include <mutex>
#include <sstream>

#ifdef __linux__

#include <libgen.h>

#endif

struct InternalThreadSafeLogStream
{

 public:
  InternalThreadSafeLogStream(const char* filename);
  ~InternalThreadSafeLogStream();
  void thread_safe_write(std::string s);

 private:
  std::mutex lock_;
  std::ofstream stream_;
};

struct LogStream
{

 public:
  LogStream(InternalThreadSafeLogStream& internal_log_stream) :
          internal_logs_stream_(internal_log_stream)
  {
  }

  LogStream(const LogStream& log_stream) :
          internal_logs_stream_(log_stream.internal_logs_stream_)
  {
  }

  ~LogStream()
  {
  }

  template<class t>
  LogStream& operator<<(t value)
  {
    buffer_ << value;
    return *this;
  }

  void flush()
  {
    internal_logs_stream_.thread_safe_write(buffer_.str());
  }

 private:
  InternalThreadSafeLogStream& internal_logs_stream_;
  std::ostringstream buffer_;

};

LogStream get_log();

inline std::string LogMessage()
{
  return "";
}

inline std::string LogMessage(const char* c)
{
  return std::string(c);
}

inline std::string LogMessage(boost::format fmt)
{
  return fmt.str();
}

template<typename TValue, typename... TArgs>
inline
std::string LogMessage(boost::format& message, TValue argm, TArgs... args)
{
  message % argm;
  return LogMessage(message, args...);
}

template<typename TValue, typename... TArgs>
inline
std::string LogMessage(TValue firstarg, TArgs... following_args)
{
  boost::format message(firstarg);
  return LogMessage(message, following_args...);
}

std::string GetLogTimeStamp();
std::string GetFileName(const char* path);

#define GET_LOGLINE(level, line, file, function, ...) {                     \
  std::hash<std::thread::id> hasher;                                        \
  std::stringstream ss;                                                     \
  ss << line;                                                               \
  std::string p = std::string(file);                                        \
  std::string full_filename = GetFileName(file);                            \
  std::string file_and_line = full_filename + ":" + ss.str() + " ";         \
  LogStream logstream = get_log();                                          \
  logstream << boost::format("%-24s") % GetLogTimeStamp();                  \
  logstream << boost::format("%-21d") % hasher(std::this_thread::get_id()); \
  logstream << boost::format("%-8s") % level;                               \
  logstream << boost::format("%-30s") % file_and_line;                      \
  logstream << boost::format("%-45s") % function;                           \
  logstream << LogMessage(__VA_ARGS__);                                     \
  logstream << "\n";                                                        \
  logstream.flush();                                                        \
}

class ScopedLogger
{
 public:
  ScopedLogger(int line, const char* file, const char* function)
  {
    line_ = line;
    file_ = file;
    function_ = std::string("<- ") + std::string(function);
  }

  ~ScopedLogger()
  {
    GET_LOGLINE("INFO", line_, file_, function_, "");
  }

 private:
  int line_;
  const char* file_;
  std::string function_;
};

template<typename ... Args>
void write_logline(int line, const char* file, std::string function, Args ... args)
{
  GET_LOGLINE("INFO", line, file, function, args...);
}

inline void force_semicolon(){}
#define stringify(x) #x

#ifdef WIN32
#define LOG_INFO(...)     GET_LOGLINE("INFO",    __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), __VA_ARGS__)
#define LOG_WARNING(...)  GET_LOGLINE("WARNING", __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), __VA_ARGS__)
#define LOG_ERROR(...)    GET_LOGLINE("ERROR",   __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), __VA_ARGS__)
#define LOG_ASSERT(x, ...)   { GET_LOGLINE("ASSERT",   __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__) + std::string(" ") + std::string(stringify(!(x)))), __VA_ARGS__) } 
#define LOG_SCOPED(...)   write_logline(__LINE__, __FILE__, std::string("-> ") + std::string(__FUNCTION__), __VA_ARGS__); \
                          ScopedLogger yttrrgfdgh(__LINE__, __FILE__, __FUNCTION__); // Some name you probably won't use as a variable name.

// Debug
#ifndef NDEBUG 
#define MICAPP_ASSERT(x, ...) if(!(x)) LOG_ASSERT(x, __VA_ARGS__); assert(x)
#endif

// Release
#ifdef NDEBUG
#define MICAPP_ASSERT(x, ...)   if(!(x)) LOG_ASSERT(x, __VA_ARGS__)
#endif

#endif

#ifdef __linux__


// Debug
#ifndef NDEBUG
#define LOG_DEBUG(...)    GET_LOGLINE("DEBUG",    __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), ##__VA_ARGS__); force_semicolon()
#define LOG_SCOPED(...)   write_logline(__LINE__, __FILE__, std::string("-> ") + std::string(__FUNCTION__), "" __VA_ARGS__); \
                          ScopedLogger yttrrgfdgh(__LINE__, __FILE__, __FUNCTION__)  // Some name you probably won't use as a variable name.
#define LOG_INFO(...)     GET_LOGLINE("INFO",    __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), ##__VA_ARGS__); force_semicolon()
#define LOG_WARNING(...)  GET_LOGLINE("WARNING", __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), ##__VA_ARGS__); force_semicolon()
#define LOG_ERROR(...)    GET_LOGLINE("ERROR",   __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), ##__VA_ARGS__); force_semicolon()
#define TOOLS_ASSERT(x, ...) if(!(x)) { GET_LOGLINE("ASSERT",   __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__) + std::string(" ") + std::string(stringify(!(x)))), ##__VA_ARGS__) }; assert(x); force_semicolon()
#endif

// Release
#ifdef NDEBUG
#define LOG_DEBUG(...)
#define LOG_SCOPED(...)
#define LOG_INFO(...)     GET_LOGLINE("INFO",    __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), ##__VA_ARGS__); force_semicolon()
#define LOG_WARNING(...)  GET_LOGLINE("WARNING", __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), ##__VA_ARGS__); force_semicolon()
#define LOG_ERROR(...)    GET_LOGLINE("ERROR",   __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__)), ##__VA_ARGS__); force_semicolon()
#define TOOLS_ASSERT(x, ...)  if(!(x)) { GET_LOGLINE("ASSERT",   __LINE__, __FILE__, (std::string("   ") + std::string(__FUNCTION__) + std::string(" ") + std::string(stringify(!(x)))), ##__VA_ARGS__) }; force_semicolon()
#endif

#endif