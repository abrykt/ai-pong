#pragma once

#define ALL(x) (x).begin(), (x).end()
#define DISABLE_COPY_ASSIGN_MOVE(className) className(const className& other) = delete; className& operator=(const className& lhs) = delete; className(className&& other) = delete

#define TAG_UNUSED(x) (void)x
namespace tools
{


}
