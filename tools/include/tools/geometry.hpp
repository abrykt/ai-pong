#pragma once

#include <tuple>
#include <cmath>
#include <vector>
#include <cassert>

namespace tools
{

template<typename T>
using Point = std::tuple<T, T>;

template<typename T>
double point_distance(const Point<T>& origin, const Point<T>& destination)
{
  T x_origin, y_origin, x_destination, y_destination;
  std::tie(x_origin, y_origin) = origin;
  std::tie(x_destination, y_destination) = destination;
  return std::sqrt(std::pow(x_origin - x_destination, 2) + std::pow(y_origin - y_destination, 2));
}

Point<double> rotate(const Point<double>& point, double radians);

template<typename T>
Point<T> add_points(const Point<T>& p, const Point<T>& d)
{
  double new_x = std::get<0>(p) + std::get<0>(d);
  double new_y = std::get<1>(p) + std::get<1>(d);
  return std::make_tuple(new_x, new_y);
}

template<typename T>
double norm(const Point<T> p)
{
  return point_distance(Point<T>(0, 0), p);
}

template<typename T>
Point<double> normalize(const Point<T>& p)
{
  T x, y;
  std::tie(x, y) = p;
  double length = norm(p);
  return Point<double>(x / length, y / length);
}


/**
 * @brief Returns the angle between two lines in radians.
 *                         A

                       XX
                     XX
                   XX
                 XXX
                XX
              XXX
             XX  XXXX
           XX        XX    theta
         XXX          XX
       XXX             XX
      XX                X
    XXX                 X
     XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
(0, 0)                                   B
 */

template<typename T>
double angle(const Point<T>& A, const Point<T>& B)
{
  T Ax, Ay, Bx, By;
  std::tie(Ax, Ay) = A;
  std::tie(Bx, By) = B;
  return std::acos((Ax * Bx + Ay * By) / (norm(A) * norm(B)));
}

template<typename T>
double angle(const Point<T>& A)
{
  T Ax, Ay;
  std::tie(Ax, Ay) = A;
  return std::atan2(Ay, Ax);
}

// Inspired by https://stackoverflow.com/a/1968345/2996272
template<typename T>
bool get_line_intersection(const std::vector<T>& first_line_start, const std::vector<T>& first_line_end,
                           const std::vector<T>& second_line_start, const std::vector<T>& second_line_end,
                           std::vector<double>& intersection)
{
  double p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y;
  p0_x = first_line_start[0];
  p0_y = first_line_start[1];
  p1_x = first_line_end[0];
  p1_y = first_line_end[1];
  p2_x = second_line_start[0];
  p2_y = second_line_start[1];
  p3_x = second_line_end[0];
  p3_y = second_line_end[1];
  double s1_x, s1_y, s2_x, s2_y;
  s1_x = p1_x - p0_x;
  s1_y = p1_y - p0_y;
  s2_x = p3_x - p2_x;
  s2_y = p3_y - p2_y;

  double s, t;
  s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
  t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

  if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
  {
    intersection.clear();
    intersection.resize(2);
    // Collision detected
    intersection[0] = p0_x + (t * s1_x);
    intersection[1] = p0_y + (t * s1_y);
    return true;
  }

  return false; // No collision
}

template<typename T>
std::vector<T> tuple_to_vector(std::tuple<T, T> tup)
{
  return std::vector<T>{std::get<0>(tup), std::get<1>(tup)};
}

}