#pragma once

#include <vector>
#include <iterator>
#include "Eigen/Dense"
#include "gui_i.hpp"


namespace tools
{

void bool_to_color_matrix(const std::vector<std::vector<bool>>& bool_matrix, PixelMatrix& color_matrix)
{
  // TODO One could maybe force the user to pass the correct size here instead.
  color_matrix.clear();
  color_matrix.resize(bool_matrix.size());
  auto row_iter = color_matrix.begin();
  for (auto&& row : bool_matrix)
  {
    std::vector<Color> color_row(row.size());
    auto per_row_iter = color_row.begin();
    for (auto&& item : row)
    {
      *per_row_iter = item ? Black : White;
      std::advance(per_row_iter, 1);
    }
    *row_iter = color_row;
    std::advance(row_iter, 1);
  }
}

void eigen_vector_bool_to_color_matrix(const Eigen::VectorXd& bool_vector, PixelMatrix& color_matrix)
{
  assert(static_cast<size_t>(color_matrix.size() * color_matrix.at(0).size()) == static_cast<size_t>(bool_vector.size()));
  unsigned index = 0;
  for (auto&& row : color_matrix)
  {
    for (auto&& item : row)
    {
      item = bool_vector[index] ? Black : White;
      ++index;
    }
  }
}

}