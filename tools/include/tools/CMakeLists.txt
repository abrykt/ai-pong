include(helpers)
unset(FILES CACHE)
set(FILES
    algorithm_helpers.hpp
    eigen_helpers.hpp
    geometry.hpp
    gmock_helpers.hpp
    gui.hpp
    gui_helpers.hpp
    gui_i.hpp
    log.hpp
    measure_time.hpp
    random.hpp
    stl_helpers.hpp
    )


add_subdirectory(ml)

add_files_to_parent_scope(PARENT_VARIABLE SOURCE_FILES
                          FILES ${FILES})
