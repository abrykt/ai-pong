#pragma once

#include <vector>
#include "tools/algorithm_helpers.hpp"
#include "tools/eigen_helpers.hpp"
#include "eigen_typedefs.hpp"

namespace tools
{

enum class ShouldNormalize
{
  Yes,
  No
};

Vector discount_reward(const Vector& results,
                       double discount_factor,
                       ShouldNormalize do_normalize = ShouldNormalize::No)
{
  double running_add = 0;

  Vector discounted_award(results.size());

  for (auto i = results.size() - 1; i >= 0; --i)
  {
    if(!tools::are_same(results(i), 0.0))
    {
      running_add = 0;
    }
    running_add = running_add * discount_factor + results(i);
    discounted_award(i) = running_add;
  }
  return do_normalize == ShouldNormalize::Yes ? normalize(discounted_award) : discounted_award;
}

std::vector<double> discount_reward(const std::vector<double>& results,
                                    double discount_factor,
                                    ShouldNormalize do_normalize = ShouldNormalize::No)
{
  Vector eig_vec = Eigen::Map<const Eigen::VectorXd, Eigen::Unaligned>(results.data(), results.size());
  Vector discounted_results = discount_reward(eig_vec, discount_factor, do_normalize);
  return std::vector<double>(discounted_results.data(), discounted_results.data() + discounted_results.size());
}

}
