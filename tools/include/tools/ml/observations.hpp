#pragma once

#include <memory>
#include <list>
#include <tuple>
#include "tools/log.hpp"
#include "eigen_typedefs.hpp"

namespace tools
{

class ObservationWithDecision
{
 public:

  // Input, output, decision
  // TODO Maybe use the same typedef as the one one error computer.
  using ObservationType = std::tuple<std::shared_ptr<Vector>, Vector>;
  using ObservationPtr = std::shared_ptr<ObservationType>;


  explicit ObservationWithDecision()
  {
    clear();
  }

  void add_training_data(const ObservationPtr& observation)
  {
    observation_list_.push_back(observation);
  }

  ObservationPtr get_next_observation()
  {
    // TODO Maybe move a pointer so that the vector does not have to be resized all the time.
    // Look into std::deque
    ObservationPtr ptr;
    if (!observation_list_.empty())
    {
      ptr = observation_list_.front();
      observation_list_.pop_front();
    }
    else
    {
      ptr = nullptr;
    }
    return ptr;
  }

  void clear() // TODO Maybe pop the items as they are fetched in get_next_observation
  {
    observation_list_.clear();
  }

  std::shared_ptr<Vector> get_input(const ObservationType& observation)
  {
    return std::get<0>(observation);
  }


 private:

  std::list<ObservationPtr> observation_list_;

};

}

