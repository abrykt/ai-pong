#pragma once 

#include "Eigen/Dense"

namespace tools
{

typedef Eigen::VectorXd Vector;
typedef Eigen::MatrixXd Matrix;

}

