#pragma once

#include <cmath>
#include <functional>
#include <vector>

#include "tools/stl_helpers.hpp"
#include "tools/log.hpp"

#include "ml_enums.hpp"

namespace tools
{

inline double sigmoid(const double& x)
{
  return 1.0 / (1.0 + exp(-x));
}

inline double sigmoid_prim(const double& x)
{
  return sigmoid(x) * (1 - sigmoid(x));
}

inline double relu(const double& x)
{
  return x > 0 ? x : 0.0;
}

inline double relu_prim(const double x)
{
  return x > 0 ? 1.0 : 0.0;
}

inline double unity(const double x)
{
  return x;
}

inline double unity_prim(const double x)
{
  TAG_UNUSED(x);
  return 1;
}

typedef std::function<double(const double&)> ActivationFunction;
typedef std::function<double(const double&)> ActivationFunctionDerivative;

class ActivationFunctionFactory
{
 public:

  static std::vector<ActivationFunction> create_activation_functions(std::vector<ActivationFunctionType>& types)
  {
    std::vector<ActivationFunction> activation_functions;
    for (auto&& activation_function_type : types)
    {
      switch (activation_function_type)
      {
        case ActivationFunctionType::Sigmoid :
        {
          activation_functions.push_back(tools::sigmoid);
          break;
        }
        case ActivationFunctionType::Relu :
        {
          activation_functions.push_back(tools::relu);
          break;
        }
        case ActivationFunctionType::Unity :
        {
          activation_functions.push_back(tools::unity);
          break;
        }
        default:
        {
          LOG_ERROR("Unsupported activation function type: %d.", static_cast<int>(activation_function_type));
          throw std::invalid_argument("Unsupported activation function type.");
        }
      }
    }
    return activation_functions;
  }

  static std::vector<ActivationFunction> create_activation_function_prims(std::vector<ActivationFunctionType>& types)
  {
    std::vector<ActivationFunctionDerivative> activation_functions_prims;
    for (auto&& activation_function_type : types)
    {
      switch (activation_function_type)
      {
        case ActivationFunctionType::Sigmoid :
        {
          activation_functions_prims.push_back(tools::sigmoid_prim);
          break;
        }
        case ActivationFunctionType::Relu :
        {
          activation_functions_prims.push_back(tools::relu_prim);
          break;
        }
        case ActivationFunctionType::Unity :
        {
          activation_functions_prims.push_back(tools::unity_prim);
          break;
        }
        default:
        {
          throw std::invalid_argument("Unsupported activation derivative type."); // TODO
        }
      }
    }
    return activation_functions_prims;
  }
};

}

