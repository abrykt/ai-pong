#pragma once

#include "tools/ml/eigen_typedefs.hpp"

namespace tools
{

class ForwardI
{
 public:
  virtual Vector forward(Vector& input) = 0;
};

}

