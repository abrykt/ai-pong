#pragma once


namespace tools
{

class StandardOutputError
{

  /*
   * const ActivationFunctionDerivative& fprim = activation_fcn_derivative.at(layer_index - 1);
    Vector f_prim_layer = input_to_layers.at(layer_index - 1).unaryExpr(std::ref(fprim));

    const Vector& output_from_this_layer = output_from_layers.at(layer_index - 1);

    if (layer_index == num_layers)
    {
      Matrix temp = -(wanted_output - actual_output);
      delta = temp.cwiseProduct(f_prim_layer); // TODO Could maybe use layer outs only
      temp = delta * output_from_this_layer.transpose();
      delta_w.at(layer_index - 1) = temp;
    }
   */

 public:
  static Vector compute_output_error(const Vector& wanted_output,
                                     const Vector& actual_output,
                                     const Vector& input_to_last_layer,
                                     const ActivationFunctionDerivative& f_prim)
  {
    // TODO Isn't "output_from_last_layer" the same as "actual_output". Seems like no since
    // unittest fail when using "actual_output" instead of "output_from_last_layer". And ut is checked agains
    // Tensorflow which should be correct.
    Vector f_prim_layer = input_to_last_layer.unaryExpr(std::ref(f_prim));
    Matrix temp = -(wanted_output - actual_output);
    return temp.cwiseProduct(f_prim_layer);
  }
};

}