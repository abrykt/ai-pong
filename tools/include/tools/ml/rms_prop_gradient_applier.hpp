#pragma once

#include <ratio>
#include <thread>
#include "tools/stl_helpers.hpp"
#include "tools/log.hpp"
#include "eigen_typedefs.hpp"

namespace tools
{

class RmsPropGradientApplierParameters
{
 public:
  static double learning_rate;
  static double decay_rate;
};


template<typename RmsPropGradientApplierParametersType = RmsPropGradientApplierParameters>
class RmsPropGradientApplier
{
 public:
  RmsPropGradientApplier() :
          are_previous_gradients_computed(false)
  {
  }

  // TODO Look into how to use below.
  //constexpr static double learning_rate = RmsPropGradientApplierParametersType::learning_rate;
  //constexpr static double decay_rate = RmsPropGradientApplierParametersType::decay_rate;

  DISABLE_COPY_ASSIGN_MOVE(RmsPropGradientApplier);

  // http://ruder.io/optimizing-gradient-descent/index.html#rmsprop
  void apply_gradients(const std::vector<Matrix>& gradients, std::vector<Matrix>& weights)
  {
    LOG_INFO("Applying gradients.");
    std::vector<Matrix> current_gradients_squared;
    compute_gradients_squared(gradients, current_gradients_squared);

    if (!are_previous_gradients_computed)
    {
      previous_gradients_squared_ = current_gradients_squared;
      are_previous_gradients_computed = true;
    }

    auto current_iter = current_gradients_squared.begin();
    auto previous_iter = previous_gradients_squared_.begin();
    auto weigths_iter = weights.begin();
    auto grad_iter = gradients.begin();

    std::vector<std::thread> threads;
    for (; current_iter < current_gradients_squared.end(); ++current_iter)
    {
      threads.emplace_back(std::thread(&RmsPropGradientApplier::update_weigths, this,
                                       std::ref(*current_iter),
                                       std::ref(*previous_iter),
                                       std::ref(*weigths_iter),
                                       std::ref(*grad_iter)));
      ++previous_iter;
      ++weigths_iter;
      ++grad_iter;
    }

    for (auto& thread : threads)
    {
      thread.join();
    }
    previous_gradients_squared_ = std::move(current_gradients_squared);
  }

  // TODO Each iteration below can run in different threads.
  static void compute_gradients_squared(const std::vector<Matrix>& gradients, std::vector<Matrix>& gradients_squared)
  {
    // TODO could maybe force user to pass the correct size.
    gradients_squared.clear();
    gradients_squared.resize(gradients.size());
    std::transform(gradients.begin(), gradients.end(), gradients_squared.begin(), [](const Matrix& gradient)
    {
      return gradient.array() * gradient.array();
    });
  }

 private:

  void update_weigths(Matrix& current, const Matrix& previous, Matrix& w, const Matrix& gradient)
  {
    current = RmsPropGradientApplierParametersType::decay_rate * previous +
              (1 - RmsPropGradientApplierParametersType::decay_rate) * current;
    w = w.array() + (Matrix::Ones(w.rows(), w.cols()) * RmsPropGradientApplierParametersType::learning_rate).array() /
                    (current +
                     Matrix::Ones(w.rows(), w.cols()) * std::numeric_limits<double>::epsilon()).cwiseSqrt().array() *
                    gradient.array();
  }

  bool are_previous_gradients_computed;
  std::vector<Matrix> previous_gradients_squared_;
};

}
