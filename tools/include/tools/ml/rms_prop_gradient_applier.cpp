#include "rms_prop_gradient_applier.hpp"

namespace tools
{

double RmsPropGradientApplierParameters::learning_rate = 1e-3;
double RmsPropGradientApplierParameters::decay_rate = 0.99;

}

