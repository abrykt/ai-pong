#pragma once

#include <random>
#include "weigth_initializer_i.hpp"

class XavierInitializer : public WeightInitializerI
{
 public:
  XavierInitializer() :
          gen_(rd_()),
          dis_(0, 1)
  {

  }
  std::vector<std::vector<double>> init_matrix(size_t num_rows, size_t num_cols)
  {
    std::vector<std::vector<double>> v(num_rows, std::vector<double>(num_cols));
    for (auto&& row : v)
    {
      for (auto&& value : row)
      {
        value = dis_(gen_) / double(num_cols);
      }
    }
    return v;
  }

 private:
  std::random_device rd_;
  std::mt19937 gen_;
  std::normal_distribution<double> dis_;
};


