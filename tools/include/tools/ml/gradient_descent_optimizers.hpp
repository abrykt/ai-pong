#pragma once

#include "gradient_descent_optimizer.hpp"
#include "weigth_initializers.hpp"
#include "rms_prop_gradient_applier.hpp"
#include "policy_gradient.hpp"
#include "backward_algorithm.hpp"
#include "observations.hpp"

namespace tools
{

using RmsPropPolicyGradientOptimizer = GradientDescentOptimizer<
        XavierInitializer,
        RmsPropGradientApplier<RmsPropGradientApplierParameters>,
        BackwardAlgorithm,
        ObservationWithDecision,
        PolicyGradient
>;

}
