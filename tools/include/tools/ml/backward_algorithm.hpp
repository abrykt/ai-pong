#pragma once

#include <vector>
#include <tools/eigen_helpers.hpp>

#include "tools/stl_helpers.hpp"
#include "eigen_typedefs.hpp"
#include "activation_functions.hpp"
#include "standard_output_error.hpp"

namespace tools
{

class BackwardAlgorithm
{
 public:

  static void forward_prop(const std::vector<double>& input,
                           const std::vector<std::vector<std::vector<double>>>& layer_weights,
                           const std::vector<ActivationFunction>& layer_activation_functions,
                           std::vector<std::vector<double>>& layer_inputs,
                           std::vector<std::vector<double>>& layer_outputs)
  {
    using tools::std_vector_to_eigen_matrix;
    Eigen::VectorXd input_ = Eigen::Map<const Eigen::VectorXd, Eigen::Unaligned>(input.data(), input.size());

    std::vector<Matrix> layer_weights_(layer_weights.size());
    std::transform(layer_weights.begin(), layer_weights.end(), layer_weights_.begin(),
                   [](const std::vector<std::vector<double>>& std_weights)
                   {
                     Matrix temp;
                     std_vector_to_eigen_matrix(std_weights, temp);
                     return temp;
                   });

    std::vector<Vector> layer_inputs_(layer_inputs.size());
    std::transform(layer_inputs.begin(), layer_inputs.end(), layer_inputs_.begin(),
                   [](const std::vector<double>& std_values)
                   {
                     return Eigen::Map<const Eigen::VectorXd, Eigen::Unaligned>(std_values.data(),
                                                                                std_values.size());
                   });

    std::vector<Vector> layer_outputs_(layer_outputs.size());
    std::transform(layer_outputs.begin(), layer_outputs.end(), layer_outputs_.begin(),
                   [](const std::vector<double>& std_values)
                   {
                     return Eigen::Map<const Eigen::VectorXd, Eigen::Unaligned>(std_values.data(),
                                                                                std_values.size());
                   });

    forward_prop(input_, layer_weights_, layer_activation_functions, layer_inputs_, layer_outputs_);

    layer_inputs.clear();
    layer_inputs.resize(layer_inputs_.size());
    std::transform(layer_inputs_.begin(), layer_inputs_.end(), layer_inputs.begin(), [](const Vector& eigen_vec)
    {
      return std::vector<double>(eigen_vec.data(), eigen_vec.data() + eigen_vec.size());
    });

    layer_outputs.clear();
    layer_outputs.resize(layer_outputs_.size());
    std::transform(layer_outputs_.begin(), layer_outputs_.end(), layer_outputs.begin(), [](const Vector& eigen_vec)
    {
      return std::vector<double>(eigen_vec.data(), eigen_vec.data() + eigen_vec.size());
    });
  }

  static void forward_prop(const Vector& input,
                           const std::vector<Matrix>& layer_weights,
                           const std::vector<ActivationFunction>& layer_activation_functions,
                           std::vector<Vector>& layer_inputs,
                           std::vector<Vector>& layer_outputs)
  {
    layer_inputs.clear();
    layer_inputs.resize(layer_activation_functions.size());

    layer_outputs.clear();
    layer_outputs.resize(layer_activation_functions.size() + 1);
    layer_outputs.at(0) = input;

    unsigned output_index = 1;
    for (const Matrix& weights_b : layer_weights)
    {
      Vector& previous_layer_output = layer_outputs.at(output_index - 1);
      layer_inputs.at(output_index - 1) = weights_b * previous_layer_output;
      const ActivationFunction& f = layer_activation_functions.at(output_index - 1);
      layer_outputs.at(output_index) = layer_inputs.at(output_index - 1).unaryExpr(f);
      ++output_index;
    }
  }


  static void backward_prop(const std::vector<double>& actual_output,
                            const std::vector<double>& wanted_output,
                            const std::vector<std::vector<double>>& input_to_layers,
                            const std::vector<std::vector<double>>& output_from_layers,
                            const std::vector<ActivationFunctionDerivative>& activation_fcn_derivative,
                            std::vector<std::vector<std::vector<double>>>& layer_weigths,
                            std::vector<std::vector<std::vector<double>>>& delta_w)
  {
    Vector actual_output_ = Eigen::Map<const Eigen::VectorXd, Eigen::Unaligned>(actual_output.data(),
                                                                                actual_output.size());

    Vector wanted_output_ = Eigen::Map<const Eigen::VectorXd, Eigen::Unaligned>(wanted_output.data(),
                                                                                wanted_output.size());
    std::vector<Vector> input_to_layers_ = std_to_eigen(input_to_layers);
    std::vector<Vector> output_from_layers_ = std_to_eigen(output_from_layers);

    std::vector<Matrix> layer_weigths_ = std_to_eigen(layer_weigths);

    std::vector<Matrix> delta_w_;

    Vector error = tools::StandardOutputError::compute_output_error(wanted_output_,
                                                                    actual_output_,
                                                                    input_to_layers_.at(input_to_layers_.size() - 1),
                                                                    activation_fcn_derivative.at(
                                                                            activation_fcn_derivative.size() - 1));

    backward_prop(error, input_to_layers_, output_from_layers_, activation_fcn_derivative, layer_weigths_, delta_w_);

    eigen_to_std(delta_w_, delta_w);

  }

  static void backward_prop(const Vector& output_error,
                            const std::vector<Vector>& input_to_layers,
                            const std::vector<Vector>& output_from_layers,
                            const std::vector<ActivationFunctionDerivative>& activation_fcn_derivative,
                            const std::vector<Matrix>& layer_weights,
                            std::vector<Matrix>& delta_w)
  {
    int num_layers = activation_fcn_derivative.size();

    delta_w.clear();
    delta_w.resize(num_layers);

    Vector delta = output_error; // Matrix?
    for (int layer_index = num_layers; layer_index > 0; --layer_index)
    {
      const ActivationFunctionDerivative& fprim = activation_fcn_derivative.at(layer_index - 1);

      if (layer_index == num_layers)
      {
        delta_w.at(layer_index - 1) = delta * output_from_layers.at(layer_index - 1).transpose();
      }
      else
      {
        delta = (layer_weights.at(layer_index).transpose() * delta).cwiseProduct(input_to_layers.at(layer_index - 1).unaryExpr(std::ref(fprim)));
        delta_w.at(layer_index - 1) = delta * output_from_layers.at(layer_index - 1).transpose();
      }
    }

  }

};
}
