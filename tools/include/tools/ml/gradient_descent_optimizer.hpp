#pragma once

#include <vector>
#include <memory>
#include <thread>

#include "tools/eigen_helpers.hpp"
#include "tools/log.hpp"
#include "tools/ml/zero_initializer.hpp"
#include "activation_functions.hpp"
#include "eigen_typedefs.hpp"
#include "backward_algorithm.hpp"
#include "ml_enums.hpp"
#include "forward_i.hpp"

namespace tools
{


template<
        typename WeightInitializerType,
        typename WeightUpdaterType,
        typename BackwardAlgorithmType,
        typename ObservationHolderType,
        typename OutputErrorComputer
>

class GradientDescentOptimizer :
        public ForwardI,
        public WeightInitializerType,
        public WeightUpdaterType,
        public BackwardAlgorithmType,
        public ObservationHolderType,
        public OutputErrorComputer,
        public ZeroInitializer
{


 public:
  GradientDescentOptimizer(std::vector<unsigned> layer_structure,
                           std::vector<ActivationFunctionType>& activations_function_type,
                           std::vector<ActivationFunctionType>& activation_function_prims_type) :
          layer_structure_(layer_structure)
  {
    weights_ = initialize_weigths<WeightInitializerType>(layer_structure);
    layer_activation_functions_ = ActivationFunctionFactory::create_activation_functions(activations_function_type);
    layer_activation_function_prims_ = ActivationFunctionFactory::create_activation_function_prims(
            activation_function_prims_type);
    clear_gradients();
  }

  void clear_gradients()
  {
    gradients_ = initialize_weigths<ZeroInitializer>(layer_structure_);
  }

  void train()
  {
    LOG_SCOPED();
    std::vector<Matrix> delta_w = initialize_weigths<ZeroInitializer>(layer_structure_);
    typename ObservationHolderType::ObservationPtr observation;
    while (observation = ObservationHolderType::get_next_observation())
    {
      std::vector<Vector> layer_inputs;
      std::vector<Vector> layer_outputs;

      const auto& input = ObservationHolderType::get_input(*observation);

      BackwardAlgorithmType::forward_prop(*input,
                                          weights_,
                                          layer_activation_functions_,
                                          layer_inputs,
                                          layer_outputs);

      //TODO Should pass last layer error to backward algorithm instead.
      Vector error = OutputErrorComputer::compute_output_error(*observation,
                                                               layer_inputs.at(layer_inputs.size() - 1),
                                                               layer_outputs.at(layer_outputs.size() - 1),
                                                               layer_activation_function_prims_.at(
                                                                       layer_activation_function_prims_.size() - 1)
      );

      BackwardAlgorithmType::backward_prop(error,
                                           layer_inputs,
                                           layer_outputs,
                                           layer_activation_function_prims_,
                                           weights_,
                                           delta_w);

      save_gradients(gradients_, delta_w);
    }
  }

  void apply_gradients()
  {
    WeightUpdaterType::apply_gradients(gradients_, weights_);
    clear_gradients();
  }

// TODO Maybe return shared_ptr instead so that we do not have to copy.
  Vector forward(Vector& input)
  {
    std::vector<Vector> layer_inputs;
    std::vector<Vector> layer_outputs;
    BackwardAlgorithmType::forward_prop(input, weights_, layer_activation_functions_, layer_inputs, layer_outputs);
    return layer_outputs[layer_outputs.size() - 1]; // TODO Compute this before.
  }

 private:

  void save_gradients(std::vector<Matrix>& gradients, const std::vector<Matrix>& delta_w)
  {
    TOOLS_ASSERT(delta_w.size() == gradients.size(), "Gradients and delta_w needs to have the same size.");
    std::vector<Matrix>::const_iterator delta_w_iter = delta_w.begin();
    std::vector<std::thread> threads(gradients.size());
    unsigned index = 0;
    for (auto&& gradient : gradients)
    {
      auto f = [](Matrix& gradient, const Matrix& deltaw) -> void
      {
        gradient += deltaw;
      };
      threads.at(index) = std::thread(f, std::ref(gradient), std::ref(*delta_w_iter));
      ++delta_w_iter;
      ++index;
    }
    // TODO Could maybe skip waiting here and check so that no pending saves are in progress next time this gets called.
    for (auto&& thread : threads)
    {
      thread.join();
    }
  }

  template<typename Initializer>
  std::vector<Matrix> initialize_weigths(std::vector<unsigned> layer_structure)
  {
    std::vector<std::vector<std::vector<double>>> weigths;
    for (size_t i = 0; i < layer_structure.size() - 1; ++i)
    {
      int num_rows = layer_structure.at(i + 1);
      int num_column = layer_structure.at(i);
      auto w_stl = Initializer::init_matrix(num_rows, num_column);
      weigths.push_back(w_stl);
    }
    return std_to_eigen(weigths);
  }

  std::vector<Matrix> weights_;
  std::vector<unsigned> layer_structure_;
  std::vector<ActivationFunction> layer_activation_functions_;
  std::vector<ActivationFunctionDerivative> layer_activation_function_prims_;
  std::vector<Matrix> gradients_;

};

}
