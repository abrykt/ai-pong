#pragma once

#include <memory>
#include "tools/stl_helpers.hpp"
#include "eigen_typedefs.hpp"
#include "activation_functions.hpp"

namespace tools
{

class PolicyGradient
{
 public:

  static Vector compute_output_error(const std::tuple<std::shared_ptr<Vector>, Vector> pixels_and_error,
                                     const Vector& input_to_last_layer,
                                     const Vector& output_from_last_layer,
                                     const ActivationFunctionDerivative& f_prim)
  {
    TAG_UNUSED(input_to_last_layer);
    TAG_UNUSED(output_from_last_layer);
    TAG_UNUSED(f_prim);
    return std::get<1>(pixels_and_error);
  }

};

}