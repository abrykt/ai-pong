#pragma once

#include <vector>
#include <cstddef>

namespace tools
{

class ZeroInitializer //: public WeightInitializerI
{
 public:
  ZeroInitializer()
  {

  }

  std::vector<std::vector<double>> init_matrix(size_t num_rows, size_t num_cols)
  {
    std::vector<std::vector<double>> v(num_rows, std::vector<double>(num_cols));
    for (auto&& row : v)
    {
      for (auto&& value : row)
      {
        value = 0.0;
      }
    }
    return v;
  }
};

}
