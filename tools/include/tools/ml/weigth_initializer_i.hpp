#pragma once

#include <cstddef>
#include <vector>

class WeightInitializerI
{
 public:
  virtual ~WeightInitializerI()
  {

  }

  virtual std::vector<std::vector<double>> init_matrix(size_t num_rows, size_t num_cols) = 0;

};


