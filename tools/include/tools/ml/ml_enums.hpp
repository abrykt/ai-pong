#pragma once

namespace tools
{
enum class ActivationFunctionType
{
  Sigmoid,
  Relu,
  Unity
};

}


